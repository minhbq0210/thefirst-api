let mongoose = require('mongoose')
let User     = require(global.baseapp + 'models/userModel.js')

function rxController(res) { this.res = res }
rxController.prototype.validate = function(rules) {
  let rxdata   = this.res.data
  let validateResult = rxu.validate(rxdata.params, rules)  
  if (!validateResult['rxresult']) { 
    rxdata.response({success: -2, msg: 'Wrong input', rxdata: validateResult})
    return false
  }

  return true 
}

rxController.prototype.validateEmail = function(email) {
  let re = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/;
  let rxdata   = this.res.data
  if (!re.test(email)) {
    rxdata.response({success: -2, msg: 'Email invalid format'})
    return false
  } else {
    return true
  }
}

rxController.prototype.authorize = function() {
  let rxdata = this.res.data
  User.findOne({ 'authorization': rxdata.params['authorization'] }, 'fullname', (err, user) => {    
    if(err || !user || !user.fullname) { 
      rxdata.response({success: -2, msg: 'Cant authorize!'}) 
    }
  })
}

rxController.prototype.filter = function(hmodel, params, arr_inttype) {
  // Int type field
  arr_inttype = arr_inttype || ['created_at', 'updated_at', 'is_deleted', 'is_active', 'type']

  let curSearchPriceMin = 0
  let curSearchPriceMax = 500

  // Default filter first 
  let filter_default = {is_deleted: 0}
  
  for(index in params) {
    let curEle = params[index]
    if (index === 'searchprice_min') {
      curSearchPriceMin = params[index]
    }
    if (index === 'searchprice_max') {
      curSearchPriceMax = params[index]
    }

    // Search with option
    if (index.indexOf('searchoption_') == 0) {
      let curSearchTerm = index.replace('searchoption_', '')
      if (curSearchTerm != 'trademark') {
        curEle = curEle.split(',')
        let searchValue = curEle.indexOf(',') == -1 ? curEle: curEle.split("\\,",-1)
        let arr = []
        searchValue.map((key, ind) => {
          let elemMatch = {}; elemMatch[curSearchTerm + '.value'] = key
          arr.push(elemMatch)
        })
        arr = {$or: arr}
        filter_default['options'] = {$elemMatch: {$and:[arr]}}  
      } else {
        var arrparams = curEle.split(',')
        var regex = [];
        for (var i = 0; i < arrparams.length; i++) {
            regex[i] = new RegExp(arrparams[i]);
        }
        filter_default['vendor'] = {$in: regex}
      }
      
    // Search price 
    } else if (index.indexOf('searchprice_min') == 0) {
      if (!filter_default['price']) {filter_default['price'] = {}}
      let curSearchTerm = index.replace('searchprice_min', '')
      filter_default['price']['$gt'] = curEle * 1000

    // Search price 
    } else if (index.indexOf('searchprice_max') == 0) {
      if (!filter_default['price']) {filter_default['price'] = {}}
      let curSearchTerm = index.replace('searchprice_max', '')
      filter_default['price']['$lt'] = curEle * 1000

    // Search with _id
    } else if (index.indexOf('searchid_') == 0) {
      let curSearchTerm = index.replace('searchid_', '')
      filter_default[curSearchTerm] = mongoose.Types.ObjectId(curEle)

    // Search with arr _id
    } else if (index.indexOf('searcharr_') == 0) {
      let curSearchTerm = index.replace('searcharr_', '')
      if (curSearchTerm.indexOf('_id') == 0) {
        let arrid = curEle.split(',')
        if (arrid.length > 0) {
          arrid.map(objid => mongoose.Types.ObjectId(objid))
        } 
        filter_default['appdist'] = {'$in' : arrid}
        // filter_default['brandlist'] = {'$in' : arrid}
      } else {
        filter_default[curSearchTerm] = {'$in' : curEle}  
      }

    }
    else if (index.indexOf('searchbrand_') == 0) {
      let curSearchTerm = index.replace('searchbrand_', '')
      if (curSearchTerm.indexOf('_id') == 0) {
        let arrid = curEle.split(',')
        if (arrid.length > 0) {
          arrid.map(objid => mongoose.Types.ObjectId(objid))
        } 
        filter_default['brandlist'] = {'$in' : arrid}
      } else {
        filter_default[curSearchTerm] = {'$in' : curEle}  
      }
    } else if (index.indexOf('searchstatus_') === 0) {
      let curSearchTerm = index.replace('searchstatus_', '')
      filter_default[curSearchTerm] = { $in: curEle }
    } else if (index.indexOf('search_') == 0) {
      let curSearchTerm = index.replace('search_', '')
      if (arr_inttype.indexOf(curSearchTerm) > -1) {
        curEle = parseInt(curEle)
        filter_default[curSearchTerm] = curEle
      } else {
        filter_default[curSearchTerm] = new RegExp(curEle, "i")
      }
    }
  }
  if (params && curSearchPriceMin && curSearchPriceMax) {
    filter_default['price']['$gt'] = curSearchPriceMin
    filter_default['price']['$lt'] = curSearchPriceMax
  }
  return hmodel.find(filter_default)
}

rxController.prototype.cbSuccess = function(data) {
  let rxdata = this.res.data
  rxdata.response({success: 1, msg: 'Query data success!', data: data})
}

rxController.prototype.cbFailed = function(data) {
  let rxdata = this.res.data
  rxdata.response({success: 0, msg: 'Query data failed!', data: data})
}

rxController.prototype.paging = function(hmodel, params) {

  // Paging
  let pg_page = (typeof(params.pg_page) == 'undefined' || parseInt(params.pg_page) < 1 )? 1  : parseInt(params.pg_page)
  let pg_size = (typeof(params.pg_size) == 'undefined' || parseInt(params.pg_size) < 1 || parseInt(params.pg_size) > 1000)? 10 : parseInt(params.pg_size)

  // Sorting
  let st_col  = params.st_col  || 'created_at'
  let st_type = (typeof(params.st_type) == 'undefined' || parseInt(params.st_type) != 1)? -1 : 1
  let st_params = {}; st_params[st_col] = st_type

  return hmodel.limit(pg_size).skip((pg_page - 1) * pg_size).sort(st_params)
}

rxController.prototype.preUpdate = function(editables, params, arr_inttype) {
  arr_inttype = arr_inttype || ['created_at', 'updated_at', 'is_deleted', 'is_active']

  let data_update = {}
  for (index in params) {
    if (editables.indexOf(index) > -1) {
      if (arr_inttype.indexOf(index) > -1) {
        params[index] = parseInt(params[index])        
      }      
      data_update[index] = params[index]
    }    
  }  

  return data_update
}

rxController.prototype.preventDupplicate = function(model, params, ignoreId, callback) {
  ignoreId = ignoreId || 0
  
  // Ignore current data 
  if (ignoreId) {
    params['_id'] = {'$ne': mongoose.Types.ObjectId(ignoreId)}
  }

  let rxdata = this.res.data
  model.find(params, function(err, docs) {
    if (docs.length) { 
      rxdata.response({success: -2, msg: 'Dupplicate data'})
    } else {
      callback()
    }
  })

  return true
}

rxController.prototype.preventDupplicateSync = async function(model, params, ignoreId) {
  ignoreId = ignoreId || 0
  
  // Ignore current data 
  if (ignoreId) {
    params['_id'] = {'$ne': mongoose.Types.ObjectId(ignoreId)}
  }

  let rxdata = this.res.data
  let [err, docs] = await rxu.to(model.find(params))
  if (docs.length) {
    rxdata.response({success: -2, msg: 'Dupplicate data'})
    return false
  } else {
    return true
  }
}
rxController.prototype.preventDupplicatePhone = async function(model, params, ignoreId) {
  ignoreId = ignoreId || 0
  
  // Ignore current data 
  if (ignoreId) {
    params['_id'] = {'$ne': mongoose.Types.ObjectId(ignoreId)}
  }

  let rxdata = this.res.data
  let [err, docs] = await rxu.to(model.find(params))
  if (docs.length) {
    rxdata.response({success: -2, msg: 'Dupplicate phone'})
    return false
  } else {
    return true
  }
}

module.exports = rxController