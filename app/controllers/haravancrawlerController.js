// Base setup
//=======================================
let util       = require('util')
let fetch      = require('node-fetch');
let express    = require('express')
let request    = require('request')
let mongoose   = require('mongoose')
let http       = require('http')
let https      = require('https')
let path       = require('path')
let url        = require('url')
let fs         = require('fs')
  
let ProductShopee = require(global.baseapp + 'models/shopeeProductModel.js') 
let ProductDetail = require(global.baseapp + 'models/shopeeProductdetailModel.js')
let Category      = require(global.baseapp + 'models/shopeeCategoryModel.js')
let Collection    = require(global.baseapp + 'models/shopeeCollectionModel.js')
let Collect       = require(global.baseapp + 'models/collectsModel.js') 
let Option        = require(global.baseapp + 'models/optionsModel.js')

function controller(res) { this.res = res; rxController.call(this, res); global.rxdata = res.data }
util.inherits(controller, rxController)

///////////
// G E T //
///////////

controller.prototype.index = async function() {  
  var paged = 0
  var request = require('request'),
    username = 'c96c6298cb3b57dfc68397664027495f',
    password = '688c2ba20a78d32b9ed053d350ce2796',
    url_product = 'https://hangdoctumy.myharavan.com/admin/products.json?page=',
    url_page    = 'https://hangdoctumy.myharavan.com/admin/products/count.json',
    auth = 'Basic ' + new Buffer(username + ':' + password).toString('base64')

    request({  url : url_page,headers : {  "Authorization" : auth }},

      async function (err, res, body) {
      body = JSON.parse(body) 
      paged = Math.floor(body['count']/50) + 1
      for (var page = 1; page <= paged; page++) {
        let url_single = url_product+page
        crawerProduct(url_single, auth)
      }
    })    
}

function crawerProduct(url_single, auth) {
  request({ url : url_single, headers : { "Authorization" : auth }},
    async function (err, res, body) {
      if(err) {}
      if(res.statusCode !== 200 ) {}
      let tempcategoryArr = []
      body = JSON.parse(body) 
      let productArr = body.products
      let x = []
      updateValueSize = [], updateValueColor = []
          
      for (let i in productArr) {
        
        let perDataAdd = {}
        perDataAdd['appobj']       = []
        perDataAdd['appdistobj']   = []
        perDataAdd['appdist']      = []
            
        let [errCollect, collects] = await rxu.to(Collect.find({product_id: body.products[i]['id']})) 
        for (var c = 0; c <= collects.length; c++) {
            if (collects[c] !== undefined) {
                let [errCat, arrCat] = await rxu.to(Collection.findOne({id: collects[c]['collection_id']})) 
                if (arrCat !== null) { perDataAdd['appdist'].push(arrCat._id), perDataAdd['appobj'].push(arrCat._id) } 
            
            }
        }
            
        let [err, dbarrProduct] = await rxu.to(ProductShopee.find({id: productArr[i]['id']})) 
        if (dbarrProduct.length === 0 ){
          try{
            getImageProduct((productArr[i].images))
            perDataAdd['img_landscape']= productArr[i]['images'][0]['filename']
          }catch(e){
            perDataAdd['img_landscape']= 'ico_app_default.png'
          }

          perDataAdd['options'] = []
          for (let h in productArr[i]['variants']) {
              var options = {}
              for (let k in productArr[i]['options']) {
                  var tempValSize = {}, tempValColor = {}
                  let posi = productArr[i]['options'][k]['position']
                  let tempOptionName = productArr[i]['options'][k]['name'].toLowerCase()
                  options[tempOptionName]    = {}
                  options['key']      = h
                  options['amount']   = productArr[i]['variants'][h]['inventory_quantity']
                  options['data']     = productArr[i]['variants'][h]
                  options[tempOptionName]['type']  = tempOptionName
                  options[tempOptionName]['name']  = tempOptionName
                  options[tempOptionName]['value'] = productArr[i]['variants'][h]['option'+ posi] || ''
                  options[tempOptionName]['text']  = productArr[i]['variants'][h]['option' + posi] || ''

                  if (tempOptionName === 'color') {
                    tempValColor['key'] = productArr[i]['variants'][h]['option'+ posi] || ''
                    tempValColor['value'] = productArr[i]['variants'][h]['option'+ posi] || ''
                  }
                  if (tempOptionName === 'size') {
                    tempValSize['key'] = productArr[i]['variants'][h]['option'+ posi] || ''
                    tempValSize['value'] = productArr[i]['variants'][h]['option'+ posi] || ''
                  }
              }

              perDataAdd['options'].push(options)
          }
          if (updateValueColor.length === 0) {
            updateValueColor.push(tempValColor)
          } else {
            var found = updateValueColor.some(function (el) {
            return el.key === tempValColor['key']
            })
            if (!found) { updateValueColor.push(tempValColor)}
          }

          if (updateValueSize.length === 0) {
            updateValueSize.push(tempValSize)
          } else {
            var found = updateValueSize.some(function (el) {
            return el.key === tempValSize['key']
            })
            if (!found) { updateValueSize.push(tempValSize)}
          }

          // console.log(updateValueColor)
          perDataAdd['image_list'] = []
          for (let l in productArr[i]['images']) {
              perDataAdd['image_list'].push(productArr[i]['images'][l]['filename'])
          }

          perDataAdd['tags']         = []
          if (productArr[i]['tags'] != null) {
              productArr[i]['tags'].split(',').map((i, k) => {
                  tempT = {}
                  tempT['id'] = k
                  tempT['text'] = i
                  perDataAdd['tags'].push(tempT)
              })
          }
          perDataAdd['is_active']    = 1
          perDataAdd['is_deleted']   = 0
          perDataAdd['__v']          = 0
          perDataAdd['stock']        = 0
          perDataAdd['rule_time']    = ''
          perDataAdd['filter_type']  = ''
          perDataAdd['seotitle']     = ''
          perDataAdd['seometa']      = ''
          perDataAdd['desc']         = ''
          perDataAdd['content']      = productArr[i]['body_html']
          perDataAdd['product_cate'] = productArr[i]['product_type']
          perDataAdd['created_at']   = Math.round(new Date(productArr[i]['created_at']).getTime()/1000)
          perDataAdd['img_portrait'] = productArr[i]['img_portrait']
          perDataAdd['images']       = productArr[i]['images']
          perDataAdd['name']         = productArr[i]['title']
          perDataAdd['slug']         = productArr[i]['handle']
          perDataAdd['id']           = productArr[i]['id']
          perDataAdd['price']        = productArr[i]['variants'][0]['price']
          perDataAdd['published_at'] = productArr[i]['published_at']
          perDataAdd['attributes']   = productArr[i]['variants']
          perDataAdd['vendor']       = productArr[i]['vendor']
          perDataAdd['body_plain']   = productArr[i]['body_plain']
          perDataAdd['template_suffix'] = productArr[i]['template_suffix']
          perDataAdd['published_scope'] = productArr[i]['published_scope']
          perDataAdd['only_hide_from_list'] = productArr[i]['only_hide_from_list']
          tempcategoryArr.push(perDataAdd)
        }
       
        updateOption('color', updateValueColor)
        updateOption('size', updateValueSize)


      }
      ProductShopee.collection.insert(tempcategoryArr,  {keepGoing: true, continueOnError: true}, function(err, result) {})
  })
}

function updateOption(name, value) {
  let query = { 'type': name }, update = { '$set': {'value': value }}
  Option.collection.update(query, update)
}

function getImageProduct(images) {
  var srcpath = 'upload/image/'
  var data = images
  var uriTemp = []
  var download = function(uri, filename, callback) {
      var req = https.request(uri, function(res) {
          var file = fs.createWriteStream(srcpath + filename)
            res.pipe(file)
          })
          req.on('error', function(e) {
            console.log('error: ' + e.message)
          })
      req.end()    
  }
  for (var j in data) {
    var img_src = images[j]['filename'] 
    var filepath = images[j]['src'].replace('http', 'https')
    download(filepath, img_src, function() {
      console.log('downloaded', img_src, j)
    })
  }
  var error = function(message) {
    console.log(message)
  }
}

controller.prototype.getCategory = async function() {  
  var request = require('request'),
    username = "c96c6298cb3b57dfc68397664027495f",
    password = "688c2ba20a78d32b9ed053d350ce2796",
    urls = "https://hangdoctumy.myharavan.com/admin/custom_collections.json",
    auth = "Basic " + new Buffer(username + ":" + password).toString("base64")

  request(
    {
        url : urls,
        headers : {
            "Authorization" : auth
        }
    },
    async function (err, res, body) {
        if(err) {}
        if(res.statusCode !== 200 ) {}
        var tempcategoryArr = []
        body = JSON.parse(body) 
        for (var i in body.custom_collections) {
            let [err, dbarrProduct] = await rxu.to(Collection.find({id: body.custom_collections[i]['id']})) 
            if (dbarrProduct.length === 0 ){
                var perDataAdd = {}
                perDataAdd['updated_at'] = body.custom_collections[i]['updated_at']
                perDataAdd['created_at'] = Math.round(new Date(body.custom_collections[i]['updated_at']).getTime()/1000)
                perDataAdd['is_active']  = 1
                perDataAdd['is_deleted'] = 0
                perDataAdd['icons']      = body.custom_collections[i]['image']
                perDataAdd['desc']       = body.custom_collections[i]['template_suffix']
                perDataAdd['name']       = body.custom_collections[i]['title']
                perDataAdd['__v']        = 0
                perDataAdd['slug']       = body.custom_collections[i]['handle']
                perDataAdd['id']         = body.custom_collections[i]['id']
                perDataAdd['products_count'] = body.custom_collections[i]['products_count']
                tempcategoryArr.push(perDataAdd)
            }
        }
    Collection.collection.insert(tempcategoryArr,  {keepGoing: true, continueOnError: true}, function(err, result) {})
  })
}

controller.prototype.getCollection = async function() { 
  console.log('fuxk') 
  var paged = 0
  var request = require('request'),
      username = 'c96c6298cb3b57dfc68397664027495f',
      password = '688c2ba20a78d32b9ed053d350ce2796',
      url_collect = 'https://hangdoctumy.myharavan.com/admin/collects.json?page=',
      url_col_count = 'https://hangdoctumy.myharavan.com/admin/collects/count.json'
      auth = "Basic " + new Buffer(username + ":" + password).toString("base64")
  request(
      {   
          url : url_col_count,
          headers : {
              "Authorization" : auth
          }
      },
      function (err, res, page) {
          page = JSON.parse(page) 
          paged = Math.floor(page['count']/50) + 1
          console.log(paged)
          for (var page = 1; page <= paged; page++) {
            request(
                {
                    url : url_collect + page,
                    headers : {
                        "Authorization" : auth
                    }
                },
                async function (err, res, body) {
                    if(err) {}
                    if(res.statusCode !== 200 ) {}
                    var tempcategoryArr = []
                    body = JSON.parse(body) 
                    for (var i in body.collects) {
                        let [err, dbarrProduct] = await rxu.to(Collect.find({id: body.collects[i]['id']})) 
                        if (dbarrProduct.length === 0 ){
                            var perDataAdd = {}
                            perDataAdd['id']            = body.collects[i]['id']
                            perDataAdd['position']      = body.collects[i]['position']
                            perDataAdd['product_id']    = body.collects[i]['product_id']
                            perDataAdd['sort_value']    = body.collects[i]['sort_value']
                            perDataAdd['collection_id'] = body.collects[i]['collection_id']
                            perDataAdd['is_active']  = 1
                            perDataAdd['is_deleted'] = 0
                            perDataAdd['updated_at'] = body.collects[i]['updated_at']
                            perDataAdd['created_at'] = Math.round(new Date(body.collects[i]['updated_at']).getTime()/1000)
                            tempcategoryArr.push(perDataAdd)
                        }
                    }
                Collect.collection.insert(tempcategoryArr,  {keepGoing: true, continueOnError: true}, function(err, result) {})
            })
          }
  })
}

module.exports = controller
