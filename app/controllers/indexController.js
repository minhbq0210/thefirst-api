let util = require('util')
let rxdata = {}

function controller(res) { this.res = res; rxController.call(this, res); rxdata = res.data }
util.inherits(controller, rxController)

controller.prototype.index = function () {  
  rxdata.response({msg: 'From index'})
}

controller.prototype.user  = function () {  
  rxdata.response({msg: 'From user'}) 
}

module.exports = controller