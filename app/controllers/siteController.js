let util     = require('util')
let mongoose = require('mongoose')
const memcache = require('memory-cache'); 
let Model    = require(global.baseapp + 'models/productcateModel.js')
let ModelProduct = require(global.baseapp + 'models/productModel.js')
let ModelOption  = require(global.baseapp + 'models/optionsModel.js')
let ModelProductCat    = require(global.baseapp + 'models/productcateModel.js')
let ModelAdvCate    = require(global.baseapp + 'models/advcateModel.js')
let ModelContent = require(global.baseapp + 'models/contentModel.js')
let ModelContentCat = require(global.baseapp + 'models/contentcateModel.js')
let ModelPage    = require(global.baseapp + 'models/pageModel.js')
let ModelConfig   = require(global.baseapp + 'models/configModel.js')

let ModelOrder = require(global.baseapp + 'models/orderModel.js')
let ModelSlide = require(global.baseapp + 'models/slideModel.js')
let ModelBanner= require(global.baseapp + 'models/bannerModel.js')

let nodemailer = require('nodemailer')
let UserModel = require(global.baseapp + 'models/userModel.js') // (rxu.orm)

let rxdata   = {}

function controller(res) { this.res = res; rxController.call(this, res); rxdata = res.data }
util.inherits(controller, rxController)

///////////
// G E T //
///////////
controller.prototype.index = async function() {  
  try {
    let key = 'homepage'
    let cachedBody = memcache.get(key)
    if (cachedBody && cachedBody.cates.length > 0 && cachedBody.menu.length > 0) {
      // this.cbSuccess(cachedBody)
    } else {
      let sectionData = {}
      let selectItem = {name: 1, nameEng: 1, price: 1, price_discount: 1, img_landscape: 1, slug: 1, is_hot: 1, desc: 1}
      if (!rxdata.params.searchid_appdist) { delete rxdata.params.searchid_appdist }
      let [err1, dbarrCate]          = await rxu.to(ModelProductCat.find({is_deleted: 0, is_active: 1, is_brand: 0, type: {$ne: 1}},selectItem).limit(20).sort({ 'priority': -1 }).exec()) //, type: 0
      let [err2, dbarrProduct]       = await rxu.to(this.paging(ModelProduct.find({is_deleted: 0, is_active: 1},selectItem), rxdata.params).exec())
      let [err3, dbarrProductbest]   = await rxu.to(this.paging(ModelProduct.find({is_deleted: 0, is_active: 1, is_hot: 1},selectItem), rxdata.params).exec())
      let [err4, dbarrSlide]         = await rxu.to(ModelSlide.find({ is_deleted: 0, is_active: 1, type: '1' }, selectItem))
      let [err5, dbarrSlideHome]     = await rxu.to(ModelSlide.find({ is_deleted: 0, is_active: 1, type: '3' }, selectItem))
      let [err6, dbarrBrand]         = await rxu.to(ModelProductCat.find({is_deleted: 0, is_active: 1, is_brand: 1},selectItem).limit(20).sort({ 'priority': -1 }).exec())
      let [err7, dbarrMenu]          = await rxu.to(ModelProductCat.find({is_deleted: 0, is_active: 1, type: 1, child: 0},selectItem).limit(8).sort({ 'priority': -1 }).exec())
      let [err8, dbarrBanner]        = await rxu.to(ModelSlide.find({is_deleted: 0, is_active: 1, type: 0},selectItem))
      let [err9, dbarrAdvertisement] = await rxu.to(ModelAdvCate.find({is_deleted: 0, is_active: 1},{content: 0}).populate({path: 'appdistobj', select: {_id: 1, name: 1, img_landscape: 1, slug: 1}}).limit(2))
      let [err10, dbarrVideo]        = await rxu.to(ModelSlide.find({is_deleted: 0, is_active: 1, type: '6'}).limit(1))
      let [err11, dbarrNews]         = await rxu.to(ModelContent.find({is_deleted: 0, is_active: 1},selectItem).limit(4))
      
      sectionData = {
        cates:  dbarrCate, 
        products: dbarrProduct, 
        productsbest: dbarrProductbest, 
        imgslides: dbarrSlide, 
        imgmobile: dbarrSlideHome, 
        brand: dbarrBrand,
        menu: dbarrMenu,
        banner: dbarrBanner,
        advertisement: dbarrAdvertisement,
        video: dbarrVideo ? dbarrVideo[0]: {},
        news: dbarrNews,
      }
      if(err1 || err2 || err3 || err4 || err5 || err6 || err7 || err8 || err9 || err10 || err11) {
        this.cbFailed()
      } else {
        // memcache.put(key, sectionData, 3600000);
        this.cbSuccess(sectionData)
      }
      
    }
  } catch (e) {
      console.log(e)
      this.cbFailed()
  }
}

controller.prototype.product = async function() {
  if (rxdata.params.searchid_appdist == '0') { delete rxdata.params.searchid_appdist }
  if (rxdata.params.searchid_brandlist == '0') { delete rxdata.params.searchid_brandlist }
  let paramssearch = {}
  for (let key in rxdata.params) {
    if (['st_full','st_col','st_type','pg_page','pg_size','timeseed'].indexOf(key) === -1) { paramssearch[key] = rxdata.params[key] }
  }
  let [errcat1, dbarrcat1] = []
  if(rxdata.params.searchid_appdist){
    [errcat1, dbarrcat1]  = await rxu.to(ModelProductCat.findOne({_id: rxdata.params.searchid_appdist, is_deleted: 0, is_active: 1 }))
  }
  if(rxdata.params.searchid_brandlist){
    [errcat1, dbarrcat1]  = await rxu.to(ModelProductCat.findOne({_id: rxdata.params.searchid_brandlist, is_deleted: 0, is_active: 1 }))
  }
  if (dbarrcat1 && dbarrcat1.trace) {
    let arrcatid = []
    let queryfilter = {trace: new RegExp(dbarrcat1.trace, "i"), is_deleted: 0, is_active: 1}
    let [errcat2, dbarrcat2] = await rxu.to(ModelProductCat.find(queryfilter)) 
    dbarrcat2.forEach(objcat => {arrcatid.push(objcat._id)})

    if (arrcatid.length > 0) {
      rxdata.params['searcharr_appdist'] = arrcatid
      if (rxdata.params['searchname_appdist']) {
        delete rxdata.params['searchid_appdist']
        delete rxdata.params['searchname_appdist']
      }
      rxdata.params['searcharr_brandlist'] = arrcatid
      if (rxdata.params['searchname_brandlist']) {
        delete rxdata.params['searchid_brandlist']
        delete rxdata.params['searchname_brandlist']
      }
    }
    let [errpro3, dbarrpro3] = await rxu.to(this.paging(this.filter(ModelProduct, rxdata.params), rxdata.params).find({ is_deleted: 0, is_active: 1 }).populate('appdistobj appobj', null, { is_deleted: 0, is_active: 1 }).exec())
    let [errcount, countarrpro] = await rxu.to(this.filter(ModelProduct, rxdata.params).find({ is_deleted: 0, is_active: 1 }).populate('appdistobj appobj', null, { is_deleted: 0, is_active: 1 }).count().exec())
    errcat2 ? this.cbFailed() : this.cbSuccess({products: dbarrpro3, count: countarrpro})
  } else {
    let [err1, dbarrProduct] = await rxu.to(this.paging(this.filter(ModelProduct, rxdata.params), rxdata.params).find({ is_deleted: 0, is_active: 1 }).populate('appdistobj appobj', null, { is_deleted: 0, is_active: 1 }).exec())
    let [err3, countProduct] = await rxu.to(this.filter(ModelProduct, rxdata.params).find({ is_deleted: 0, is_active: 1}).populate('appdistobj appobj', null, { is_deleted: 0, is_active: 1 }).count().exec())
    err1 ? this.cbFailed() : this.cbSuccess({products: dbarrProduct, count: countProduct})
  } 
}

controller.prototype.option = async function() {
  let [err, dbarr] = await rxu.to(this.paging(this.filter(ModelOption, rxdata.params), rxdata.params).exec())
  err ? this.cbFailed() : this.cbSuccess(dbarr)
}

controller.prototype.config = async function() {
  let [err, dbarr] = await rxu.to(ModelConfig.find({is_deleted: 0},{info: 1, _id: 0}).exec())
  let result = {currency: [], shipping: []}
  if (dbarr && dbarr[0] && dbarr[0]['info'] && dbarr[0]['info']['currency']) {
    result['currency'] = dbarr[0]['info']['currency']
  }
  if (dbarr && dbarr[0] && dbarr[0]['info'] && dbarr[0]['info']['shipping']) {
    result['shipping'] = dbarr[0]['info']['shipping']
  }
  err ? this.cbFailed() : this.cbSuccess(result)
}

controller.prototype.header = async function() {
  try {
    let key = 'header'
    let cachedBody = memcache.get(key)
    if (cachedBody && cachedBody.cates.length > 0 && cachedBody.menu.length > 0) {
      // this.cbSuccess(cachedBody)
    } else {
      let sectionData = {}
      let [err, dbarrConfig] = await rxu.to(ModelConfig.find({is_deleted: 0},{info: 1, _id: 0}).exec())
      let result = {currency: [], shipping: []}
      if (dbarrConfig && dbarrConfig[0] && dbarrConfig[0]['info'] && dbarrConfig[0]['info']['currency']) {
        result['currency'] = dbarrConfig[0]['info']['currency']
      }
      if (dbarrConfig && dbarrConfig[0] && dbarrConfig[0]['info'] && dbarrConfig[0]['info']['shipping']) {
        result['shipping'] = dbarrConfig[0]['info']['shipping']
      }

      let [err1, dbarCates] = await rxu.to(ModelProductCat.find({is_deleted: 0, is_brand: 0, type: {$ne: 1}}).sort({ 'priority': -1 })) //, type: 0
      let [err2, dbarMenuCate] = await rxu.to(ModelProductCat.find({is_deleted: 0, type: 1, child: 0}).sort({ 'priority': -1 }))
      let [err3, dbarMenu] = await rxu.to(ModelProductCat.find({is_deleted: 0, type: 1, child: 1}).sort({ 'priority': -1 }))
      
      if(dbarMenuCate && dbarMenuCate.length > 0) {
        for(let i = 0; i < dbarMenuCate.length; i++) {
          let arr = dbarMenu.filter(o => o.appdist && o.appdist.indexOf(dbarMenuCate[i]._id) !== -1)
          dbarMenuCate[i].childArr = arr
        } 
      }

      sectionData = {
        cates: dbarCates, 
        config: result,
        menu: dbarMenuCate
      }
      if(err || err1) {
        this.cbFailed()
      } else {
        // memcache.put(key, sectionData, 3600000);
        this.cbSuccess(sectionData)
      }
      
    }
  } catch (e) {
      console.log(e)
      this.cbFailed()
  }
}

controller.prototype.productdetail = async function() {
  console.log(rxdata.params.slug, 'slug')
  let valid = this.validate({
    slug: {required: true},
  })

  if (valid) {
    let [err, dbarrProduct] = await rxu.to(ModelProduct.findOne({slug: rxdata.params['slug']}).populate('appdistobj appobj', null, { is_deleted: 0, is_active: 1 }))
    let [err2, dbarrCate] = await rxu.to(this.paging(this.filter(Model, {is_active: 1, is_deleted: 0}), {}).exec())
    err || err2 ? this.cbFailed() : this.cbSuccess({product: dbarrProduct, categories: dbarrCate})
  }
}

controller.prototype.news = async function() {
  let [err1, dbarrContent] = await rxu.to(this.paging(this.filter(ModelContent, rxdata.params), rxdata.params).populate('appdistobj appobj', null, { is_deleted: 0, is_active: 1 }).exec())
  let [err2, dbarrContentCate] = await rxu.to(this.paging(this.filter(ModelContentCat, {is_active: 1, is_deleted: 0}), {pg_size: 9999}).exec())
  let [err3, dbarrContentHot]  = await rxu.to(this.paging(this.filter(ModelContent, {is_active: 1, is_deleted: 0, is_hot: 1}), {pg_size: 10}).exec())
  err1 || err2 || err3 ? this.cbFailed() : this.cbSuccess({contents: dbarrContent, categories: dbarrContentCate, contentshot: dbarrContentHot})
}

controller.prototype.newsdetail = async function() {
  let valid = this.validate({
    slug: {required: true},
  })

  if (valid) {
    let [err1, dbarrNews] = await rxu.to(ModelContent.findOne({slug: rxdata.params['slug']}).populate('appdistobj appobj', null, { is_deleted: 0, is_active: 1 }))
    let [err2, dbarrContentCate] = await rxu.to(this.paging(this.filter(ModelContentCat, {is_active: 1, is_deleted: 0}), {pg_size: 9999}).exec())
    let [err3, dbarrContentHot]  = await rxu.to(this.paging(this.filter(ModelContent, {is_active: 1, is_deleted: 0, is_hot: 1}), {pg_size: 10}).exec())

    if(dbarrNews && dbarrNews.ratings) {
      dbarrNews.ratings.sort((a, b) => new Date(b.created_at*1000) - new Date(a.created_at*1000))
    }

    err1 || err2 || err3 ? this.cbFailed() : this.cbSuccess({news: dbarrNews, categories: dbarrContentCate, contentshot: dbarrContentHot})
  }
}

controller.prototype.pages = async function() {
  let [err1, dbarrContent] = await rxu.to(this.paging(this.filter(ModelPage, rxdata.params), rxdata.params).populate('appdistobj appobj', null, { is_deleted: 0, is_active: 1 }).exec())
  let [err3, dbarrContentHot]  = await rxu.to(this.paging(this.filter(ModelPage, {is_active: 1, is_deleted: 0, is_hot: 1}), {pg_size: 10}).exec())
  err1 || err3 ? this.cbFailed() : this.cbSuccess({contents: dbarrContent, contentshot: dbarrContentHot})
}

controller.prototype.pagesdetail = async function() {
  let valid = this.validate({
    slug: {required: true},
  })

  if (valid) {
    let [err1, dbarrPages] = await rxu.to(ModelPage.findOne({slug: rxdata.params['slug']}).populate('appdistobj appobj', null, { is_deleted: 0, is_active: 1 }))
    let [err3, dbarrContentHot]  = await rxu.to(this.paging(this.filter(ModelPage, {is_active: 1, is_deleted: 0, is_hot: 1}), {pg_size: 10}).exec())
    err1 || err3 ? this.cbFailed() : this.cbSuccess({pages: dbarrPages, contentshot: dbarrContentHot})
  }
}

controller.prototype.order = async function() {
  let valid = this.validate({
    orderid: {required: true, alphaNumeric: true},
  })
  if (valid) {
    let [err1, dbarrOrder] = await rxu.to(ModelOrder.findOne({order_code: rxdata.params['orderid']}))
    let [err2, dbarrCate] = await rxu.to(this.paging(this.filter(Model, {is_active: 1, is_deleted: 0}), {}).exec())
    err1 ? this.cbFailed() : this.cbSuccess({order: dbarrOrder, categories: dbarrCate})
  } else {
    rxdata.response({success: -2, msg: 'Wrong input'})
  }  
}

controller.prototype.sentemail = async function() {
  let useremail = rxdata.params.email


  try {
    if (validateEmail(useremail)) {
      let transporter = nodemailer.createTransport({
        service: 'gmail',
        auth: {
          user: 'dpashostline@gmail.com',
          pass: 'ecdcadmin'
        }
      })
      let useridmask = makeid()
      let username = ''//(userInfo.fullname) ? userInfo.fullname : userInfo.username
      let htmlbody = '<p><b>Xin chào</b></p>' +
            '<p>Đây là email thông báo bạn đã đăng ký nhận bản tin và các thông tin sản phẩm mới nhất của chúng tôi.</p>' +
            '<p>Chúng tôi sẽ gửi cho bạn thông tin sản phẩm khi có sản phẩm mới.</p>' + 
            '<p>Để đăng ký tài khoản hãy truy cập vào <a href="http://annaweb.bonanhem.com/register">Đăng ký tài khoản</a> </p>' + 
            '<p>Cảm ơn bạn đã sử dụng dịch vụ của chúng tôi </p>'

      let mailOptions = {
        from: 'dpashostline@gmail.com',
        to: useremail,
        subject: 'Đăng ký nhận bản tin và các thông tin về sản phẩm mới nhất',
        html: htmlbody
      }
      
      transporter.sendMail(mailOptions, function (error, info) {    
        if (error) {
          console.log(error)
        } else {
          console.log('Email sent: ' + info.response)
        }
      })

      this.cbSuccess({success: 1})
    } else {
      this.cbSuccess({msg: 'Email không đúng định dạng'})
    }  
  } catch(e) {
    this.cbSuccess({success: 1, msg: e.message})
  }

}

function validateEmail(email) {
  var re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
  return re.test(String(email).toLowerCase());
}

function makeid () {
  let text = ''
  let possible = 'ABCDEFGHIJKLMNOPQRSTUVWXYZ'
  for (let i = 0; i < 7; i++) {
    text += possible.charAt(Math.floor(Math.random() * possible.length))
  }

  return text
}

function subString (string) {
  var startText = string.slice(0, 100)
  return startText + '...'
}

module.exports = controller