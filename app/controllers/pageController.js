let util     = require('util')
let mongoose = require('mongoose')
let Model    = require(global.baseapp + 'models/pageModel.js')
let User     = require(global.baseapp + 'models/userModel.js')
let rxdata   = {}

function controller(res) { this.res = res; rxController.call(this, res); rxdata = res.data }
util.inherits(controller, rxController)

///////////
// G E T //
///////////
controller.prototype.index = async function() {      
  let [err, dbarr] = await rxu.to(this.paging(this.filter(Model, rxdata.params), rxdata.params).populate('appdistobj appobj', null, { is_deleted: 0, is_active: 1 }).exec())
  err ? this.cbFailed() : this.cbSuccess(dbarr)
}

controller.prototype.delete = async function() {  
  let [err, dbarr] = await rxu.to(Model.findByIdAndUpdate(rxdata.params['_id'], {is_deleted: 1}, {new: false}))
  err ? this.cbFailed() : this.cbSuccess(dbarr)
}

controller.prototype.restore = async function() {  
  let [err, dbarr] = await rxu.to(Model.findByIdAndUpdate(rxdata.params['_id'], {is_deleted: 0}, {new: false}))
  err ? this.cbFailed() : this.cbSuccess(rxdata.params['_id'])
}

/////////////
// P O S T //
/////////////
controller.prototype.POSTindex = async function() {  
  let updating  = [ 'name', 'desc', 'content',
                    'slug', 'seotitle', 'seometa', 'price', 'stock',
                    'filter_type', 'appdist', 'appdistobj', 
                    'img_landscape', 'img_portrait', 'img_detail',
                    'rule_time', 'is_active', 'is_hot' ]

  let modeldata = this.preUpdate(updating, rxdata.params)  
  modeldata.slug     = modeldata.slug || modeldata.name.replace(/[^-a-zA-Z0-9\s+]+/ig, '').replace(/\s+/gi, "-").toLowerCase()
  modeldata.seotitle = modeldata.seotitle || modeldata.name
  modeldata.seometa  = modeldata.seometa || modeldata.desc  
  modeldata.appdist  = modeldata.appdist != null ? ((modeldata.appdist.constructor == Array)? modeldata.appdist : modeldata.appdist.split(',')) : null
  modeldata.appdistobj = modeldata.appdist

  // Check if this is valid _id
  for (let i in modeldata.appdist) {
    if (!mongoose.Types.ObjectId.isValid(modeldata.appdist[i])) {
      modeldata.appdist = null
      modeldata.appdistobj = null
      break
    }
  }

  let dbObj = new Model(modeldata)
  let [err, redbObj] = await rxu.to(dbObj.save())
  err ? this.cbFailed() : this.cbSuccess(redbObj)
}

controller.prototype.POSTedit = async function() {  
  let updating = ['name', 'desc', 'content',
                  'slug', 'seotitle', 'seometa', 'price', 'stock',
                  'filter_type', 'appdist', 'appdistobj',
                  'img_landscape', 'img_portrait', 'img_detail',
                  'rule_time', 'is_active', 'is_hot']

  let modeldata = this.preUpdate(updating, rxdata.params)  
  modeldata.slug   = modeldata.slug || modeldata.name.replace(/[^-a-zA-Z0-9\s+]+/ig, '').replace(/\s+/gi, "-").toLowerCase()
  modeldata.seotitle = modeldata.seotitle || modeldata.name
  modeldata.seometa  = modeldata.seometa || modeldata.desc
  modeldata.appdist  = modeldata.appdist != null ? ((modeldata.appdist.constructor == Array)? modeldata.appdist : modeldata.appdist.split(',')) : null
  modeldata.appdistobj = modeldata.appdist

  // Check if this is valid _id
  for (let i in modeldata.appdist) {
    if (!mongoose.Types.ObjectId.isValid(modeldata.appdist[i])) {
      modeldata.appdist = null
      modeldata.appdistobj = null
      break
    }
  }

  let [err, dbObj] = await rxu.to(Model.findByIdAndUpdate(rxdata.params['_id'], modeldata, {new: false}))
  err ? this.cbFailed() : this.cbSuccess(rxdata.params['_id'])
}

module.exports = controller