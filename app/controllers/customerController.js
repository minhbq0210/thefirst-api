let util     = require('util')
let mongoose = require('mongoose')
let Model    = require(global.baseapp + 'models/customerModel.js')
let OrderModel = require(global.baseapp + 'models/orderModel.js')
let ProductModel = require(global.baseapp + 'models/productModel.js')
let UserModel    = require(global.baseapp + 'models/userModel.js')

function controller(res) { this.res = res; rxController.call(this, res); global.rxdata = res.data }
util.inherits(controller, rxController)

///////////
// G E T //
///////////
controller.prototype.index = async function() {  
  let [err, dbarr] = await rxu.to(this.paging(this.filter(Model, rxdata.params), rxdata.params).populate('appdistobj appobj').exec())
  err ? this.cbFailed() : this.cbSuccess(dbarr)
}

controller.prototype.delete = async function() {  
  console.log(rxdata.params)
  let [err, dbarr] = await rxu.to(Model.findByIdAndUpdate(rxdata.params['_id'], {is_deleted: 1}, {new: false}))
  err ? this.cbFailed() : this.cbSuccess(rxdata.params['_id'])
}

controller.prototype.restore = async function() {  
  let [err, dbarr] = await rxu.to(Model.findByIdAndUpdate(rxdata.params['_id'], {is_deleted: 0}, {new: false}))
  err ? this.cbFailed() : this.cbSuccess(rxdata.params['_id'])
}

controller.prototype.allorder = async function() {  
  let paramsorder = rxdata.params
  let paramscustomer = {is_deleted: 0}
  if (rxdata.params.ids_order !== undefined) {
    paramsorder['_id'] = rxdata.params.ids_order.split(',')
  } else {
    paramsorder['_id'] = []
  } 
  if (rxdata.params.customerid !== undefined) { 
    paramscustomer['_id'] = mongoose.Types.ObjectId(rxdata.params.customerid)
    let [err, docs] = await rxu.to(Model.findOne(paramscustomer))
    if (docs) {
      paramsorder['_id'] = docs.ids_order
    } else {
      paramsorder['_id'] = []
    }
  }
  if (paramsorder['_id'].length > 0) {
    if(paramsorder['_id'] !== 'undefined'){
      let arrayorder = paramsorder['_id'].map((item) => mongoose.Types.ObjectId(item))
      let [err, dbarr] = await rxu.to(this.paging(OrderModel.find({'_id': {'$in' : arrayorder}}), paramsorder).populate('appdistobj appobj').exec())
      err ? this.cbFailed() : this.cbSuccess(dbarr)
    }
  } else {
    this.cbSuccess([])
  }
}

controller.prototype.allproduct = async function() {  
  let paramsorder = paramscustomer = paramsproduct = {is_deleted: 0}
  let arrayproductids = [] 
  if (rxdata.params.customerid !== undefined) {  
    paramscustomer['_id'] = mongoose.Types.ObjectId(rxdata.params.customerid)
    let [errorder, dbarrorder] = await rxu.to(Model.findOne(paramscustomer))
    paramsorder['_id'] = dbarrorder ? dbarrorder.ids_order : []
    let [err, dbarr] = await rxu.to(OrderModel.find(paramsorder))
    if (dbarr) {
      let arrcartstmp = dbarr.map((item) => Object.keys(JSON.parse(item.detail)['carts']))
      let arrayproducttmp = [] 
      for(var key in arrcartstmp){
        arrayproducttmp = arrayproducttmp.concat(arrcartstmp[key])
      }
      for(var key in arrayproducttmp){
        var productid = arrayproducttmp[key].split('|')[0]
        if ( arrayproductids.indexOf(productid) == -1 && productid.length > 3) {
          arrayproductids.push(productid)
        }
      }
    } 
  }
  let [errproduct, dbarrproduct] = await rxu.to(this.paging(ProductModel.find({'_id': {'$in' : arrayproductids}, is_deleted: 0}), rxdata.params))
  errproduct ? this.cbFailed() : this.cbSuccess(dbarrproduct)  
}

controller.prototype.POSTedit = async function() { 
  // console.log(rxdata.params)
  // let [errUser, dbUser] = await rxu.to(UserModel.findOne({fullname: rxdata.params.name})) //, phone: rxdata.params.phone
  // if(dbUser){
  //   let [err, dbarr] = await rxu.to(UserModel.findByIdAndUpdate(dbUser['_id'], {customerid: rxdata.params['_id']}, {new: false}))
  //   err ? this.cbFailed() : this.cbSuccess(rxdata.params['_id'])
  // }

  let updating = ['name', 'phone', 'address']
  let intfield = ['created_at', 'updated_at', 'is_deleted', 'is_active']
  let filterArr= ['password']
  let modeldata = this.preUpdate(updating, rxdata.params)
  let [err, dbObj] = await rxu.to(Model.findByIdAndUpdate(rxdata.params['_id'], modeldata, {new: true}))
  err ? this.cbFailed() : dbObj ? this.cbSuccess(rxu.filter(dbObj, filterArr)) : this.cbFailed()

  // let address = rxdata.params['address']
  // let [err, dbarr] = await rxu.to(Model.findByIdAndUpdate(rxdata.params['_id'], {address: address}, {new: false}))
  // err ? this.cbFailed() : this.cbSuccess(rxdata.params['_id'])
}

module.exports = controller


