let util     = require('util')
let mongoose = require('mongoose')
let Model    = require(global.baseapp + 'models/configModel.js')
let rxdata   = {}

function controller(res) { this.res = res; rxController.call(this, res); rxdata = res.data }
util.inherits(controller, rxController)

///////////
// G E T //
///////////
controller.prototype.index = async function() {      
  let [err, dbarr] = await rxu.to(this.paging(this.filter(Model, rxdata.params), rxdata.params).populate('appdistobj appobj', null, { is_deleted: 0, is_active: 1 }).exec())
  err ? this.cbFailed() : this.cbSuccess(dbarr)
}

controller.prototype.delete = async function() {  
  let [err, dbarr] = await rxu.to(Model.findOneAndUpdate({type : rxdata.params['type']}, { $pull: { 'meta': { 'id': rxdata.params['id'] }}}, {new: false}))
  err ? this.cbFailed() : this.cbSuccess(dbarr)
}

controller.prototype.restore = async function() {  
  let [err, dbarr] = await rxu.to(Model.findByIdAndUpdate(rxdata.params['_id'], {is_deleted: 0}, {new: false}))
  err ? this.cbFailed() : this.cbSuccess(rxdata.params['_id'])
}

/////////////
// P O S T //
/////////////
controller.prototype.POSTindex = async function() {  
  let modeldata = {}
  modeldata['type'] = rxdata.params['type']
  modeldata['name'] = rxdata.params['name']
  modeldata['desc'] = rxdata.params['desc']
  modeldata['created_at'] = modeldata['updated_at'] = Math.floor(Date.now() / 1000)
  modeldata['is_deleted'] = 0
  let [err, dbarr] = await rxu.to(Model.findOneAndUpdate({type : rxdata.params['type']},{ $set: modeldata, $addToSet: { meta: rxdata.params['meta']} }, {new: true, upsert: true}))
  err ? this.cbFailed() : this.cbSuccess(dbarr)
}

controller.prototype.POSTsetting = async function() {  
  let modeldata = {}
  modeldata['type'] = rxdata.params['type']
  modeldata['name'] = rxdata.params['name']
  modeldata['desc'] = rxdata.params['desc']
  modeldata['info'] = rxdata.params['info']
  modeldata['meta'] = []
  modeldata['created_at'] = modeldata['updated_at'] = Math.floor(Date.now() / 1000)
  modeldata['is_deleted'] = 0
  let [err, dbarr] = await rxu.to(Model.findOneAndUpdate({type : rxdata.params['type']},{ $set: modeldata}, {new: true, upsert: true}))
  err ? this.cbFailed() : this.cbSuccess(dbarr)
}

controller.prototype.POSTedit = async function() {  
  let modeldata = {}
  modeldata['meta.$.metadata'] = rxdata.params['meta']['metadata']
  let [err, dbarr] = await rxu.to(Model.findOneAndUpdate({type : rxdata.params['type'], 'meta.id' : rxdata.params['meta']['id']},{ $set: modeldata }, {new: true, upsert: true}))
  err ? this.cbFailed() : this.cbSuccess(dbarr)
}

module.exports = controller