let util     = require('util')
let mongoose = require('mongoose')
var request    = require('request')
let Model    = require(global.baseapp + 'models/orderModel.js')
let CustomerModel = require(global.baseapp + 'models/customerModel.js')
let ProductModel = require(global.baseapp + 'models/productModel.js')
let DiscountModel = require(global.baseapp + 'models/discountModel.js')

function controller(res) { this.res = res; rxController.call(this, res); global.rxdata = res.data }
util.inherits(controller, rxController)

///////////
// G E T //
///////////
controller.prototype.index = async function() {  
  var apiLink = 'http://api.serverapi.host/api/v1/apiv3/CalculateFee'
  request.post({
    headers: {'content-type' : 'application/json'},
    url:     apiLink,
    json: {
      "token": "TokenTest",
      "Weight": 500,
      "FromDistrictID": 1444,
      "ToDistrictID": rxdata.params.district,
      "ServiceID": 53320,
      "OrderCosts": [
          {
              "ServiceID": 100012
          },
          {
              "ServiceID": 100008
          }
      ],
      "CouponCode": "",
      "InsuranceFee": 9000000
    }
  }, (error, response, body) => {
    this.cbSuccess(body)
  });
  
  // this.cbSuccess([])
}

controller.prototype.getDistricts = async function() {  
  var apiLink = 'http://api.serverapi.host/api/v1/apiv3/GetDistricts'
  request.post({
    headers: {'content-type' : 'application/json'},
    url:     apiLink,
    json: {token: 'TokenTest'}
  }, (error, response, body) => {
    this.cbSuccess(body)
  });
}

controller.prototype.getServices = async function() {  
  var apiLink = 'http://api.serverapi.host/api/v1/apiv3/FindAvailableServices'
  request.post({
    headers: {'content-type' : 'application/json'},
    url:     apiLink,
    json: {
      'token': 'TokenTest',
      'Weight': 10000,
      'Length': 10,
      'Width': 110,
      'Height': 20,
      'FromDistrictID': 1455,
      'ToDistrictID': 1462,
      'CouponCode': '',
      'InsuranceFee': 9000000,
    }
  }, (error, response, body) => {
    this.cbSuccess(body)
  });
}

module.exports = controller


