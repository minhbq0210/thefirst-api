
      <center>
        <div class='rxsent-mail'>
          <div class='rxtitle-mail'>ANNA STORE</div>
          <hr class='rx-sent-mail-hr'/>
          <div class='rx-body-mail'>
            <div class='rx-mail-header'>
              <div class='rx-header-mail-title'>Chào <b>Ân</b></div>
              <div class='rx-header-mail-title'>Cám ơn bạn đã đặt hàng tại <b>Anna Store!</b> Chúng tôi gửi email này cho bạn để xác nhận đơn hàng <b>#1000474490</b> vừa đặt</div>
              <div class='rx-header-mail-title'>Mã đơn hàng: <b>#1000474490</b></div>
            </div>
            <table class='rx-table-mail'>
              <thead>
                <tr>
                  <th class='rx-body-mail-image'>Hình ảnh</th>
                  <th class='rx-body-mail-name'>Tên sản phẩm</th>
                  <th class='rx-body-mail-amount'>Số lượng</th>
                  <th class='rx-body-mail-price'>Đơn giá</th>
                  <th class='rx-body-mail-price'>Giảm giá</th>
                  <th class='rx-body-mail-price'>Tổng cộng</th>
                </tr>
              </thead>
              <tbody class='rx-body-mail'>
                <tr class='rx-body-mail-product'>
                  <td>
                    <div><img src="https://ci6.googleusercontent.com/proxy/aMknJmyafVRjRP-LO4hnSSiKBYXQD84kr8VndoKZocxh52g5CalknK5QAjjU6FxR56IdnxsJ_WuyRqmrY-1CtPdo-D3ryt_eCoTjhOqSeiNyE7lviriYHTzaXZ2vR6EzemL8tYiOu67KUD-OvP0bzx72=s0-d-e1-ft#http://product.hstatic.net/1000006063/product/upload_8e4e3253c79b42e7a40005563625f628_small.jpg"/></div>
                  </td>
                  <td>
                    <div>[Pony's Choice] [Best Seller] Phấn Phủ Siêu Mịn, Giúp Che Phủ Lỗ Chân Lông Và Khuyết Điểm Eglips Blur Powder Pact (không tích điểm) - #23 - da hơi ngăm</div>
                  </td>
                  <td>1</td>
                  <td>145,000₫</td>
                  <td>145,000₫</td>
                  <td>145,000₫</td>
                </tr>
                <tr class='rx-body-mail-product'>
                  <td>
                    <div><img src="https://ci6.googleusercontent.com/proxy/aMknJmyafVRjRP-LO4hnSSiKBYXQD84kr8VndoKZocxh52g5CalknK5QAjjU6FxR56IdnxsJ_WuyRqmrY-1CtPdo-D3ryt_eCoTjhOqSeiNyE7lviriYHTzaXZ2vR6EzemL8tYiOu67KUD-OvP0bzx72=s0-d-e1-ft#http://product.hstatic.net/1000006063/product/upload_8e4e3253c79b42e7a40005563625f628_small.jpg"/></div>
                  </td>
                  <td>
                    <div>[Pony's Choice] [Best Seller] Phấn Phủ Siêu Mịn, Giúp Che Phủ Lỗ Chân Lông Và Khuyết Điểm Eglips Blur Powder Pact (không tích điểm) - #23 - da hơi ngăm</div>
                  </td>
                  <td>1</td>
                  <td>145,000₫</td>
                  <td>145,000₫</td>
                  <td>145,000₫</td>
                </tr>
              </tbody>
            </table>
            <hr class='rx-sent-mail-hr'/>
            <div class='rx-total-mail'>
              <div class='adform_ct1'><span>Tổng giá trị sản phẩm:</span><span>145000</span></div>
              <div class='adform_ct1'><span>Khuyến mãi:</span><span>0</span></div>
              <div class='adform_ct1'><span>Phí vận chuyển:</span><span>0</span></div>
              <div class='adform_ct1'><span>Tổng cộng:</span><span>145000</span></div>
            </div>
            <div class='rx-bottom-mail'>
              <div class='adform_ct1'><span>Ngày đặt hàng:</span><span>0</span></div>
              <div class='adform_ct1'><span>Phương thức thanh toán:</span><span>0</span></div>
              <div class='adform_ct1'><span>Phương thức giao hàng:</span><span>0</span></div>
              <div class='adform_ct1'><span>Địa chỉ giao hàng:</span><span>0</span></div>
            </div>
          </div>
        </div>
        <div class='rx-footer-mail'>
          <div class=''>Đây là email xác nhận đơn hàng tự động từ Anna Store, đội ngũ nhân viên chúng tôi sẽ liên lạc với bạn để xác nhận trong thời gian sớm nhất. Xin cảm ơn đã đặt hàng tại website Anna Store!</div>
          <div class=''>© 2019 <b>Anna House.</b> Terms  |  Privacy</div>
        </div>
      </center>