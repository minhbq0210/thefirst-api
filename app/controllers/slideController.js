let util     = require('util')
let mongoose = require('mongoose')
let Model    = require(global.baseapp + 'models/slideModel.js')
let rxdata   = {}

function controller(res) { this.res = res; rxController.call(this, res); rxdata = res.data }
util.inherits(controller, rxController)

///////////
// G E T //
///////////
controller.prototype.index = async function() {
  let [err, dbarr] = await rxu.to(this.paging(this.filter(Model, rxdata.params), rxdata.params).exec())
  err ? this.cbFailed() : this.cbSuccess(dbarr)
}
controller.prototype.logo = async function() {
  rxdata.params.type = '4'
  let [err, dbarr] = await rxu.to(Model.findOne({'type': '4', 'is_deleted': 0}))
  err ? this.cbFailed() : this.cbSuccess(dbarr)
}

controller.prototype.delete = async function() {  
  let [err, dbarr] = await rxu.to(Model.findByIdAndUpdate(rxdata.params['_id'], {is_deleted: 1}, {new: false}))
  err ? this.cbFailed() : this.cbSuccess(dbarr)
}

controller.prototype.restore = async function() {  
  let [err, dbarr] = await rxu.to(Model.findByIdAndUpdate(rxdata.params['_id'], {is_deleted: 0}, {new: false}))
  err ? this.cbFailed() : this.cbSuccess(rxdata.params['_id'])
}

/////////////
// P O S T //
/////////////
controller.prototype.POSTindex = async function() {  
  let updating  = [ 'name','desc','url','img_landscape','is_active', 'type']
  let modeldata = this.preUpdate(updating, rxdata.params)  
  // Check if this is valid _id
  let dbObj = new Model(modeldata)
  let [err, redbObj] = await rxu.to(dbObj.save())
  err ? this.cbFailed() : this.cbSuccess(redbObj)
}

controller.prototype.POSTedit = async function() {  
  let updating = [ 'name','desc','url','img_landscape','is_active', 'type']

  let modeldata = this.preUpdate(updating, rxdata.params)  
  let [err, dbObj] = await rxu.to(Model.findByIdAndUpdate(rxdata.params['_id'], modeldata, {new: false}))
  err ? this.cbFailed() : this.cbSuccess(rxdata.params['_id'])
}

module.exports = controller