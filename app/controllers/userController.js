let util     = require('util')
let mongoose = require('mongoose')
let Model    = require(global.baseapp + 'models/userModel.js')
let CustomerModel = require(global.baseapp + 'models/customerModel.js')
let crypto   = require('crypto')
let rxdata   = {}

let nodemailer = require('nodemailer')
const Email = require('email-templates')

let filterArr= ['password']

function controller(res) { this.res = res; rxController.call(this, res); rxdata = res.data }
util.inherits(controller, rxController)

controller.prototype.index = async function() {        
  let [err, dbarr] = await rxu.to(this.paging(this.filter(Model, rxdata.params), rxdata.params).exec())
  err ? this.cbFailed() : this.cbSuccess(dbarr)
}

controller.prototype.delete = async function() {  
  let [err, dbobj] = await rxu.to(Model.findByIdAndUpdate(rxdata.params['_id'], {is_deleted: 1}, {new: false}))
  err ? this.cbFailed() : this.cbSuccess(rxdata.params['_id'])
}

controller.prototype.restore = async function() {  
  let [err, dbobj] = await rxu.to(Model.findByIdAndUpdate(rxdata.params['_id'], {is_deleted: 0}, {new: false}))
  err ? this.cbFailed() : this.cbSuccess(rxdata.params['_id'])
}
controller.prototype.sentcode = async function() {  
  let useremail = rxdata.params.email
  if (this.validateEmail(useremail)) {
    let [err, dbobj] = await rxu.to(Model.findOne({'email':useremail}))      
    if(dbobj) {
      try {
        let transporter = nodemailer.createTransport({
          service: 'gmail',
          auth: {
            user: 'annastore189@gmail.com',
            pass: 'Honganhotran123'
          }
        })
        let useridmask = makeid()
        let username = ''
        let htmlbody = '<p><b>Xin chào</b></p>' +
              '<p>Australiachemistwarehouse đã nhận được yêu cầu thay đổi mật khẩu của quý khách.</p>' +
              '<p>Xin hãy click vào đường dẫn sau để đổi mật khẩu: <a href="http://www.annawholesale.com.au/reset_password/'+dbobj._id+'">annawholesale.com.au/reset_password</a></p>' + 
              '<p>Cảm ơn bạn đã sử dụng dịch vụ của chúng tôi </p>'

        let mailOptions = {
          from: 'annastore189@gmail.com',
          to: useremail,
          subject: 'Yêu cầu thay đổi mật khẩu',
          html: htmlbody
        }
        
        transporter.sendMail(mailOptions, function (error, info) {    
          if (error) {
            console.log(error)
          } else {
            console.log('Email sent: ' + info.response)
          }
        })

        this.cbSuccess({success: 1})  
      } catch(e) {
        this.cbSuccess({Failed: -1, msg: e.message})
      }
    } else {
      rxdata.response({success: -2, msg: 'Not Found Account'})
    }
  }else {
    rxdata.response({success: -2, msg: 'Email invalid format'})
  }
}

controller.prototype.POSTindex = async function() {
  // console.log(rxdata.params)
  if (this.validate({
    firstname: {required: true}, 
    lastname: {required: true},
    email: {required: true},
    phone: {required: true},
    address: {required: true},
    password: {required: true}
  })) {

    rxdata.params.password = rxu.md5(rxdata.params.password)
    rxdata.params.fullname = rxdata.params.lastname + ' ' + rxdata.params.firstname

    let valid = await this.preventDupplicateSync(Model, {email: rxdata.params['email']}, rxdata.params['_id'])
    let validPhone = await this.preventDupplicatePhone(Model, {phone:rxdata.params['phone']}, rxdata.params['_id'])
    let valieemail = this.validateEmail(rxdata.params.email)
    if (valid && valieemail && validPhone) {
      let [errcustomer, dbcustomer] = await rxu.to(CustomerModel.findOneAndUpdate(
        { name : rxdata.params['fullname'], phone : rxdata.params['phone'] }, 
        { $set: {is_member: true}},
        { new: true, upsert: true}
      ))
      // console.log(dbcustomer)

      rxdata.params['customerid'] = (dbcustomer) ? dbcustomer._id : ''
      let updating  = ['firstname', 'lastname', 'fullname', 'email', 'phone', 'password', 'address', 'customerid']
      let modeldata = this.preUpdate(updating, rxdata.params) 
      let dbObj = new Model(modeldata)
      let [err, dbarr] = await rxu.to(dbObj.save())
      err ? this.cbFailed() : this.cbSuccess(dbarr)
    } 
  }
}

controller.prototype.POSTadmin = async function() {
  if (this.validate({
    fullname: {required: true}, 
    email: {required: true},
    username: {required: true},
    phone: {required: true},
    address: {required: true},
    password: {required: true},
    roleid: {required: true},
  })) { 
    rxdata.params.password = rxu.md5(rxdata.params.password)
    rxdata.params.admin = true

    let updating  = ['fullname', 'email', 'phone', 'password', 'address', 'admin', 'roleid', 'username']
    let modeldata = this.preUpdate(updating, rxdata.params) 
    // modeldata.roleobj = modeldata.roleid
    let dbObj = new Model(modeldata)
    let valid = await this.preventDupplicateSync(Model, {email: rxdata.params['email']}, rxdata.params['_id'])
    let valieemail = this.validateEmail(rxdata.params.email)
    if (valid && valieemail) {
      let [err, dbarr] = await rxu.to(dbObj.save())
      err ? this.cbFailed() : this.cbSuccess(dbarr)
    } 
  }
}

controller.prototype.POSTedit = async function() {  
  let updating = ['fullname', 'email', 'phone', 'address', 'roleid', 'username']
  let intfield = ['created_at', 'updated_at', 'is_deleted', 'is_active']

  let valid = await this.preventDupplicateSync(Model, {email: rxdata.params['email']}, rxdata.params['_id'])
  if (valid) {
    let modeldata = this.preUpdate(updating, rxdata.params)
    // modeldata.roleobj = modeldata.roleid
    let [err, dbObj] = await rxu.to(Model.findByIdAndUpdate(rxdata.params['_id'], modeldata, {new: false}))
    err ? this.cbFailed() : this.cbSuccess(rxdata.params['_id'])
  }
}
controller.prototype.POSTreset_password = async function() {  
  // console.log(rxdata.params)
  rxdata.params['id'] = mongoose.Types.ObjectId(rxdata.params['id'])
  rxdata.params['password'] = rxu.md5(rxdata.params['password'])
  let [errr, dbobj] = await rxu.to(Model.findOne({'_id': rxdata.params['id']}))
  if(dbobj) {
    let [err1, dbObj1] = await rxu.to(Model.findByIdAndUpdate(rxdata.params['id'], {password: rxdata.params['password']}, {new: false}))
    err1 ? this.cbFailed() : dbObj1 ? this.cbSuccess(dbObj1) : this.cbFailed()
  }   
}

controller.prototype.POSTeditcustomer = async function() {  
  let updating = ['fullname', 'phone', 'address', 'img_landscape']
  let intfield = ['created_at', 'updated_at', 'is_deleted', 'is_active']
  let filterArr= ['password']
  let modeldata = this.preUpdate(updating, rxdata.params)
  console.log(modeldata, 'modeldata')
  let [err, dbObj] = await rxu.to(Model.findByIdAndUpdate(rxdata.params['_id'], modeldata, {new: true}))
  console.log(dbObj, 'dbObj')
  err ? this.cbFailed() : dbObj ? this.cbSuccess(rxu.filter(dbObj, filterArr)) : this.cbFailed()
}
function makeid () {
  let text = ''
  let possible = 'ABCDEFGHIJKLMNOPQRSTUVWXYZ'
  for (let i = 0; i < 7; i++) {
    text += possible.charAt(Math.floor(Math.random() * possible.length))
  }

  return text
}

module.exports = controller