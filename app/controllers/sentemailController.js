/* global rxController, rxu */
let util = require('util')
let nodemailer = require('nodemailer')
let UserModel = require(global.baseapp + 'models/userModel.js') // (rxu.orm)
let OrderModel = require(global.baseapp + 'models/orderModel.js')
const Email = require('email-templates')

let rxdata = {}
function controller (res) { this.res = res; rxController.call(this, res); rxdata = res.data }
util.inherits(controller, rxController)

/// ////////
// G E T //
/// ////////
controller.prototype.index = async function () {
  let useremail = rxdata.params.email
  try {
    if (validateEmail(useremail)) {
      let transporter = nodemailer.createTransport({
        service: 'gmail',
        auth: {
          user: 'annawholesale01@gmail.com',
          pass: 'Wholesale01'
        }
      })
      let useridmask = makeid()
      let username = ''//(userInfo.fullname) ? userInfo.fullname : userInfo.username
      let htmlbody = '<p><b>Xin chào</b></p>' +
            '<p>Đây là email thông báo bạn đã đăng ký nhận bản tin và các thông tin sản phẩm mới nhất của chúng tôi.</p>' +
            '<p>Chúng tôi sẽ gửi cho bạn thông tin sản phẩm khi có sản phẩm mới.</p>' + 
            '<p>Để đăng ký tài khoản hãy truy cập vào <a href="http://australiachemistwarehouse.com.au/register">Đăng ký tài khoản</a> </p>' + 
            '<p>Cảm ơn bạn đã sử dụng dịch vụ của chúng tôi </p>'

      let mailOptions = {
        from: 'annawholesale01@gmail.com',
        to: useremail,
        subject: 'Đăng ký nhận bản tin và các thông tin về sản phẩm mới nhất',
        html: htmlbody
      }
      
      transporter.sendMail(mailOptions, function (error, info) {    
        if (error) {
          console.log(error)
        } else {
          console.log('Email sent: ' + info.response)
        }
      })

      this.cbSuccess({success: 1})
    } else {
      this.cbSuccess({msg: 'Email không đúng định dạng'})
    }  
  } catch(e) {
    this.cbSuccess({success: 1, msg: e.message})
  }
}

controller.prototype.orderdetail = async function () {
  let useremail = rxdata.params.email
  let attachments = [{ filename: 'customEmail.js', path: path.join(__dirname, 'customEmail.js'), contentType: 'text/javascript' }];
  try {
    if (validateEmail(useremail)) {
      let transporter = nodemailer.createTransport({
        service: 'gmail',
        auth: {
          user: 'annawholesale01@gmail.com',
          pass: 'Wholesale01'
        }
      })
      let useridmask = makeid()
      let username = ''//(userInfo.fullname) ? userInfo.fullname : userInfo.username
      let htmlbody = '<p><b>Xin chào</b></p>' +
                    '<p class="testclass">Đây là email thông báo bạn vừa đặt hàng thành công </p>' +
                    '<p>Vui lòng click vào link dưới đây để xem chi tiết:</p>' +
                    'http://australiachemistwarehouse.com.au/customer/order'

      let mailOptions = {
        from: 'annawholesale01@gmail.com',
        to: useremail,
        subject: '[AnnaHouse] - Thông báo đặt hàng thành công',
        html: htmlbody
      }
      
      transporter.sendMail(mailOptions, function (error, info) {    
        if (error) {
          console.log(error)
        } else {
          console.log('Email sent: ' + info.response)
        }
      })

      this.cbSuccess({success: 1})
    } else {
      this.cbSuccess({msg: 'Email không đúng định dạng'})
    }  
  } catch(e) {
    this.cbSuccess({success: 1, msg: e.message})
  }
}

controller.prototype.test = async function () {
  let [err, data] = await rxu.to(OrderModel.findOne({order_code: rxdata.params.order_code}))
  if(data) {
    let temJson = JSON.parse(data.detail)
    let productarr = []
    let totalProduct = 0
    Object.keys(temJson.carts).forEach(objKey => {
      totalProduct += temJson.carts[objKey].option.amount * temJson.carts[objKey].option.data.price
      productarr.push(temJson.carts[objKey].option)
    })
    let type_payment = data.type_payment === 1 ? 'Thanh toán khi nhận hàng' : 'Chuyển khoản'
    let useremail = data.email
    try {
      if (validateEmail(useremail)) {
        const transporter = nodemailer.createTransport({
          service: 'gmail',
          auth: {
            user: 'annawholesale01@gmail.com',
            pass: 'Wholesale01'
          }
        });
        const email = new Email({
          transport: transporter,
          send: true,
          preview: false,
        });
        email.send({
          template: 'sentMail',
          message: {
            from: 'annawholesale01@gmail.com',
            to: useremail,
          },
          locals: {
            name: data.name,
            phone: data.phone,
            address: data.address[0],
            textorder: data.history_order[0].text,
            type_payment: type_payment,
            price: data.price.toFixed(2),
            fee_ship: data.fee_ship,
            price_discount: data.price_discount.toFixed(2),
            totalpay: (Number(data.totalpay)+Number(data.fee_ship)).toFixed(2),
            discount_bill: data.discount_bill.toFixed(2),
            totalweight: data.totalweight,
            productarr: productarr,
            code: data.order_code,
            totalProduct: totalProduct.toFixed(2),
            date: rxgetdatetime(data.created_at)
          }
        }).then(() => {
          console.log('email has been sent!')
          this.cbSuccess({success: 1, msg: 'Một email vừa được gửi đến email của bạn'})
        });
      } else {
        this.cbSuccess({msg: 'Email không đúng định dạng'})
      }  
    } catch(e) {
      this.cbSuccess({success: 1, msg: e.message})
    }
  }
}

function validateEmail(email) {
  var re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
  return re.test(String(email).toLowerCase());
}
function rxgetdatetime (timestamp) {
  timestamp = (timestamp + 25200) * 1000
  let u = new Date(timestamp)
  let tempstr = 'Ngày ' + ('0' + u.getUTCDate()).slice(-2) + ' tháng ' + ('0' + (u.getUTCMonth() + 1)).slice(-2) + ' năm ' + u.getUTCFullYear()
  return tempstr
}

function makeid () {
  let text = ''
  let possible = 'ABCDEFGHIJKLMNOPQRSTUVWXYZ'
  for (let i = 0; i < 7; i++) {
    text += possible.charAt(Math.floor(Math.random() * possible.length))
  }

  return text
}

function subString (string) {
  var startText = string.slice(0, 100)
  return startText + '...'
}

module.exports = controller
