let util     = require('util')
let mongoose = require('mongoose')
let Model    = require(global.baseapp + 'models/contentcateModel.js')
let rxdata   = {}

function controller(res) { this.res = res; rxController.call(this, res); rxdata = res.data }
util.inherits(controller, rxController)

///////////
// G E T //
///////////
controller.prototype.index = async function() {    
  let [err, dbarr] = await rxu.to(this.paging(this.filter(Model, rxdata.params), rxdata.params).exec())
  err ? this.cbFailed() : this.cbSuccess(dbarr)
}

controller.prototype.delete = async function() {  
  let [err, dbobj] = await rxu.to(Model.findByIdAndUpdate(rxdata.params['_id'], {is_deleted: 1}, {new: false}))
  err ? rxdata.response({msg: 'Cant delete'}) : this.cbSuccess(rxdata.params['_id'])
}

controller.prototype.restore = async function() {  
  let [err, dbobj] = await rxu.to(Model.findByIdAndUpdate(rxdata.params['_id'], {is_deleted: 0}, {new: false}))
  err ? rxdata.response({msg: 'Cant restore'}) : this.cbSuccess(rxdata.params['_id'])
}

/////////////
// P O S T //
/////////////
controller.prototype.POSTindex = async function() {  
  let dbObj = new Model({
    name: rxdata.params['name'], 
    desc: rxdata.params['desc'],
    instoreyet: rxdata.params['instoreyet'],
    platform: rxdata.params['platform'],
    icons: rxdata.params['icons'],
    storeurl: rxdata.params['storeurl'],
    categories: rxdata.params['categories']
  })

  let valid = await this.preventDupplicateSync(Model, {name: dbObj.name, name: 'rxnotprevent'}, 0) 
  if (valid) {    
    let [err, dbObj1] = await rxu.to(dbObj.save())
    err ? this.cbFailed() : this.cbSuccess(dbObj1)
  }
}

controller.prototype.POSTedit = async function() {  
  let updating = ['name', 'desc', 'platform', 'icons', 'storeurl', 'categories', 'is_active', 'is_instore']
  let intfield = ['created_at', 'updated_at', 'is_deleted', 'is_active', 'is_instore']

  let valid = await this.preventDupplicateSync(Model, {name: rxdata.params['name']}, rxdata.params['_id']) 
  if (valid) {
    let [err, dbObj] = await rxu.to(Model.findByIdAndUpdate(rxdata.params['_id'], this.preUpdate(updating, rxdata.params, intfield), {new: false}))
    err ? this.cbFailed() : this.cbSuccess(rxdata.params['_id'])
  }
}

module.exports = controller