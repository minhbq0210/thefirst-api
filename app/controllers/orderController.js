let util     = require('util')
let mongoose = require('mongoose')
let nodemailer = require('nodemailer')
let Model    = require(global.baseapp + 'models/orderModel.js')
let CustomerModel = require(global.baseapp + 'models/customerModel.js')
let ProductModel = require(global.baseapp + 'models/productModel.js')
let DiscountModel = require(global.baseapp + 'models/discountModel.js')
const Email = require('email-templates')


function controller(res) { this.res = res; rxController.call(this, res); global.rxdata = res.data }
util.inherits(controller, rxController)

///////////
// G E T //
///////////
controller.prototype.index = async function() {  
  let [err, dbarr] = await rxu.to(this.paging(this.filter(Model, rxdata.params), rxdata.params).populate('appdistobj appobj').exec())
  let [err1, countOrder] = await rxu.to(this.filter(Model, rxdata.params).populate('appdistobj appobj').count().exec())
  err ? this.cbFailed() : this.cbSuccess({ order: dbarr, count: countOrder })
}
controller.prototype.all = async function () {
  let [err, dbarr] = await rxu.to(Model.find({ is_deleted: 0 }).exec())
  err ? this.cbFailed() : this.cbSuccess(dbarr)
}
controller.prototype.optionproduct = async function() {
  let arrresult = []

  let [err, dbarr] = await rxu.to(Model.find({'_id':mongoose.Types.ObjectId(rxdata.params.orderid), 'is_deleted': 0}))
  let arrcartstmp = dbarr.map((item) => JSON.parse(item.detail)['carts'])[0]
  let arraycartproduct = []
  for(var keycart in arrcartstmp){
    let newkeycart = arrcartstmp[keycart].id || ''
    var arrkeycart = newkeycart.split('|')
    arraycartproduct.push({'product_id': arrkeycart[0], 'option': arrkeycart[1]})
  }
  
  if (typeof(arraycartproduct) !== 'undefined' && arraycartproduct.length > 0) {
    let arrproduct_ids = arraycartproduct.map((item) => item.product_id)
    let [errProduct, dbarrProduct] = await rxu.to(ProductModel.find({ _id: { $in: arrproduct_ids }} ))
    let arrtmp = dbarrProduct.map(
      (item) => {
      var objOption = {}
      objOption['product_id'] = item._id
      objOption['option'] = item.options
      return objOption
    })

    for (var i=0; i < arrtmp.length; i++) {
      let arrtmp1 = {}
      var keytmp = arraycartproduct.filter((item) => { return item.product_id == arrtmp[i]['product_id'] })[0]['option'];
      var arroption = arrtmp[i]['option'].filter((item) => { return item.key == keytmp })[0]
      if (arroption && arroption['key']) {delete arroption['key']}
      if (arroption && arroption['amount']) {delete arroption['amount']}
      if (arroption && arroption['data']) {delete arroption['data']}
      let arroptiontmp = []
      for (let key in arroption) {
        arroptiontmp.push(arroption[key])
      }
      arrresult.push({'product_id': arrtmp[i]['product_id'], 'option': arroptiontmp})
    }
  }
  this.cbSuccess(arrresult)
}

controller.prototype.delete = async function() {  
  let [err, dbarr] = await rxu.to(Model.findByIdAndUpdate(rxdata.params['_id'], {is_deleted: 1, reason_deleted: rxdata.params.reason_deleted}, {new: false}))
  err ? this.cbFailed() : this.cbSuccess(rxdata.params['_id'])
}

controller.prototype.restore = async function() {  
  let [err, dbarr] = await rxu.to(Model.findByIdAndUpdate(rxdata.params['_id'], {is_deleted: 0}, {new: false}))
  err ? this.cbFailed() : this.cbSuccess(rxdata.params['_id'])
}

/////////////
// P O S T //
/////////////
controller.prototype.POSTindex = async function() {
  if(rxdata.params['address'] && rxdata.params['address'].length>0) {
    rxdata.params['street'] = rxdata.params['address'][0].street
    rxdata.params['county'] = rxdata.params['address'][0].county
    rxdata.params['city'] = rxdata.params['address'][0].city
  }
  if(rxdata.params['address_deliver'] && rxdata.params['address_deliver'].length>0) {
    rxdata.params['street'] = rxdata.params['address_deliver'][0].street
    rxdata.params['county'] = rxdata.params['address_deliver'][0].county
    rxdata.params['city'] = rxdata.params['address_deliver'][0].city
  }
  if (this.validate({
    name: {required: true}, 
    phone: {required: true},
    address: {required: true},
    street: {required: true},
    county: {required: true},
    city: {required: true},
  })) {
    if (rxdata.params['discountcode']) {
      let [errdiscount, dbarrdiscount] = await rxu.to(DiscountModel.findOneAndUpdate( { code: rxdata.params['discountcode']}, { $inc: { 'amountdiscountused' : 1}}, { new: true}))
      if (dbarrdiscount) {
        rxdata.params['price'] = helpCalDiscount(dbarrdiscount, rxdata.params['price'])
        rxdata.params['totalpay'] = helpCalDiscount(dbarrdiscount, rxdata.params['totalpay'])
      }
    }
    let textorder = 'Đơn hàng được đặt với hình thức '+rxdata.params['textorder']
    // if (rxdata.params['type_payment'] && Number(rxdata.params['type_payment']) == 2) {textorder = 'Đơn hàng được đặt với hình thức thanh toán chuyển khoản qua ngân hàng'}
    rxdata.params['history_order'] = [{text: rxdata.params['textorder'], time: Math.floor(Date.now() / 1000), status: 1}]
    rxdata.params['status_order'] = 1
    rxdata.params['order_code'] = randomcode()
    let updating  = ['name', 'email', 'phone', 'address', 'detail', 'price', 'is_active', 'history_order', 'status_order', 'arrcart', 'order_code', 'type_payment', 'price_discount', 'fee_ship', 'totalpay','discount_bill', 'totalweight', 'desc', 'currency']
    let modeldata = this.preUpdate(updating, rxdata.params) 
    modeldata.phone = rxdata.params['phone_deliver'] ? rxdata.params['phone_deliver'] : modeldata.phone
    modeldata.address = rxdata.params['address_deliver'] ? rxdata.params['address_deliver'] : modeldata.address
    let temJson = rxdata.params.detail
    if(typeof temJson === 'string') {temJson = JSON.parse(temJson)}
    if (Object.keys(temJson.carts).length > 0) {
      let dbObj = new Model(modeldata)
      let [err, dbarr] = await rxu.to(dbObj.save())
      let [err1, dbCustomer] = await rxu.to(CustomerModel.findOne({name : rxdata.params['name'], phone : rxdata.params['phone']})) 
      if(dbCustomer) {
        let address = dbCustomer.address
        let addresstmp = modeldata.address[0]
        let key = false
        for (let i = 0; i < address.length; i++) {
          if(address[i].street && address[i].county && address[i].city) {
            if(addresstmp.street.toLowerCase() === address[i].street.toLowerCase()
              && addresstmp.county.toLowerCase() === address[i].county.toLowerCase()
              && addresstmp.city.toLowerCase() === address[i].city.toLowerCase()){
              key = true
            }
          }
        }
        if(key === true) {rxdata.params['address'] = address}
        else {
          address.push(addresstmp)
          rxdata.params['address'] = address
        }
      }
      let savecustomer = function(arrayorder) {
        let customerorder = {}
        customerorder['name'] = rxdata.params.name
        customerorder['phone'] = rxdata.params.phone
        customerorder['address'] = rxdata.params.address
        customerorder['ids_order'] = arrayorder._id
        customerorder['price'] = arrayorder.price
        customerorder['fee_ship'] = arrayorder.fee_ship
        customerorder['price_discount'] = arrayorder.price_discount
        customerorder['totalpay'] = arrayorder.totalpay
        customerorder['discount_bill'] = arrayorder.discount_bill
        let customeradd = {}
        customeradd['is_deleted'] = 0
        customeradd['is_active'] = 1
        customeradd['created_at'] = Math.floor(Date.now() / 1000)
        customeradd['updated_at'] = Math.floor(Date.now() / 1000)
        customeradd['address'] = customerorder['address']
        CustomerModel.findOneAndUpdate( 
          { name : customerorder['name'], phone : customerorder['phone'] }, 
          { $set: customeradd,
            $inc : { price : customerorder['price']},
            $inc : { fee_ship : customerorder['fee_ship']},
            $inc : { 'price_discount' : customerorder['price_discount']},
            $inc : { 'totalpay' : customerorder['totalpay']},
            $inc : { 'discount_bill' : customerorder['discount_bill']},
            $push : {'ids_order' : customerorder['ids_order']},
          },
          { new: true, upsert: true}, 
          (err, dbarrfind) =>{
            console.log(err)
            if (err) {this.cbFailed()} 
          }
        ) 
        return arrayorder
      }
      this.sentMail(dbarr, 'order')
      err ? this.cbFailed() : this.cbSuccess(savecustomer(dbarr))
    }
    else {
      this.cbFailed({status: -4})
    }    
  }  
}

controller.prototype.POSTedit = async function() {  
  // console.log(rxdata.params.detail)
  // let tmp = await rxu.to(Model.find({'_id':mongoose.Types.ObjectId(rxdata.params['_id']), 'is_deleted': 0}))
  // console.log(tmp)
  var arrupdate = {}
  if (rxdata.params.updatestatus == 'statusConfirm') {
    let [errorder, dbarrorder] = await rxu.to(Model.find({'_id':mongoose.Types.ObjectId(rxdata.params['_id']), 'is_deleted': 0}))
    let arrcartstmp = dbarrorder.map((item) => JSON.parse(item.detail)['carts'])[0]
    let arraycartproduct = []
    for(var keycart in arrcartstmp){
      // var arrproductoption = keycart.split('|')
      var arrproductoption = arrcartstmp[keycart]['id'].split('|')
      var amount = 0 - Number(arrcartstmp[keycart]['amount'])
      let [errfind, dbarrfind] = await rxu.to(ProductModel.findOne({ '_id':mongoose.Types.ObjectId(arrproductoption[0]), 'inventory_policy': 1, 'is_deleted': 0, options: { $elemMatch: { key: arrproductoption[1], amount: { $exists: true }} }}))
      if (dbarrfind) {
        let [errProduct, dbarrProduct] = await rxu.to(ProductModel.findOneAndUpdate({ '_id':mongoose.Types.ObjectId(dbarrfind._id), options: { $elemMatch: { key: arrproductoption[1]} }}, { $inc : { 'options.$.amount' : amount}},{ new: true, upsert: false, multi: true}))
      }
    }
    arrupdate = {text: 'Đơn hàng đã được xác thực bởi người bán', time: Math.floor(Date.now() / 1000), status: 2}
    rxdata.params['status_order'] = 2
  } else if (rxdata.params.updatestatus == 'statusPayment') {
    arrupdate = {text: 'Người bán đã xác nhận đơn hàng đã thanh toán', time: Math.floor(Date.now() / 1000), status: 3}
    rxdata.params['status_order'] = 3
  } else if (rxdata.params.updatestatus == 'statusShip') {
    arrupdate = {text: 'Đơn hàng đã được vận chuyển bởi nhà vận chuyển VNPost', time: Math.floor(Date.now() / 1000), status: 4}
    rxdata.params['status_order'] = 4
  } else if (rxdata.params.updatestatus == 'statusRefund' && rxdata.params.status_order_payment == 2) {
    arrupdate = {text: 'Hoàn trả thành công ' + rxdata.params.amountrefund, time: Math.floor(Date.now() / 1000), status: 5, amountrefund: rxdata.params.amountrefund, reasonrefund: rxdata.params.reasonrefund}
    rxdata.params['status_order'] = 5
  }
  
  let updating  = ['name', 'email', 'phone', 'address', 'detail', 'price', 'is_active', 'status_confirm', 'status_ship', 'status_order_payment', 'status_COD', 'status_order', 'fee_ship','price_discount','totalpay', 'discount_bill', 'totalweight', 'arrcart']
  let modeldata = this.preUpdate(updating, rxdata.params)
  // Check if this is valid _id
  for (let i in modeldata.appdist) {
    if (!mongoose.Types.ObjectId.isValid(modeldata.appdist[i])) {
      break
    }
  }
  var updateparams = {}
  if (arrupdate && Object.keys(arrupdate).length !== 0) {
    updateparams = { $set: modeldata, $push : {'history_order' : arrupdate} }
  } else {
    updateparams = { $set: modeldata }
  }
  let [errcond, checkcond] = await rxu.to(Model.findById(rxdata.params['_id']))
  if (rxdata.params['status_order'] < 4) {
    let [err, dbarr] = await rxu.to(Model.findByIdAndUpdate(rxdata.params['_id'], updateparams, {new: false}))
    if(rxdata.params.updatestatus === 'statusConfirm' && dbarr) {this.sentMail(rxdata.params, 'edit')}
    err ? this.cbFailed() : this.cbSuccess(rxdata.params['_id'])
  } else {
    if (checkcond && checkcond.status_confirm == 1) {
      let [err, dbarr] = await rxu.to(Model.findByIdAndUpdate(rxdata.params['_id'], updateparams, {new: false}))
      if(rxdata.params.updatestatus === 'statusConfirm' && dbarr) {this.sentMail(rxdata.params, 'edit')}
      err ? this.cbFailed() : this.cbSuccess(rxdata.params['_id'])
    } else {
      this.cbFailed()
    }
  }
}
controller.prototype.sentMail = async function (data, name) {
  if(data) {
    let temJson = JSON.parse(data.detail)
    let productarr = []
    let totalProduct = 0 
    Object.keys(temJson.carts).forEach(objKey => {
      let total = temJson.carts[objKey].pricenew ? temJson.carts[objKey].pricenew : (temJson.carts[objKey].amount * temJson.carts[objKey].data.price)
      totalProduct += total
      temJson.carts[objKey].data.price = rxcurrencyVnd(temJson.carts[objKey].data.price, data.currency)
      temJson.carts[objKey].discount_item = rxcurrencyVnd(temJson.carts[objKey].discount_item, data.currency)
      temJson.carts[objKey].data.total = rxcurrencyVnd(total, data.currency)
      productarr.push(temJson.carts[objKey])
    })
    let type_payment = data.type_payment === 1 ? 'Khi nhận hàng' : 'Chuyển khoản'
    let useremail = data.email
    totalProduct = rxcurrencyVnd(totalProduct, data.currency)
    let fee_ship = rxcurrencyVnd(data.fee_ship, data.currency)
    let price_discount = rxcurrencyVnd(data.price_discount, data.currency)
    let totalpay = data.totalpay + data.fee_ship
    totalpay = rxcurrencyVnd(totalpay, data.currency)
    try {
      if (validateEmail(useremail)) {
        const transporter = nodemailer.createTransport({
          service: 'gmail',
          auth: {
            user: 'annawholesale01@gmail.com',
            pass: 'Wholesale01'
          }
        });
        const email = new Email({
          transport: transporter,
          send: true,
          preview: false,
        });
        email.send({
          template: 'sentMail',
          message: {
            from: 'annawholesale01@gmail.com',
            to: useremail,
          },
          locals: {
            type: name === 'order' ? 0 : 1,
            name: data.name,
            phone: data.phone,
            address: data.address[0],
            textorder: data.history_order[0].text,
            type_payment: type_payment,
            totalProduct: totalProduct,
            fee_ship: fee_ship,
            price_discount: price_discount,
            totalpay: totalpay,
            totalweight: data.totalweight,
            productarr: productarr,
            code: data.order_code,
            date: rxgetdatetime(data.created_at),
            time_delivery: 'Từ '+ rxgetdatetime(data.created_at + 259200) + ' đến '+ rxgetdatetime(data.created_at + 604800)
          }
        }).then(() => {
          console.log('email has been sent!')
          return('Một email vừa được gửi đến email của bạn')
        });
      } else {
        return('Email không đúng định dạng')
      }  
    } catch(e) {
      console.log(e,'efkeo')
      return(e.message)
    }
  }
}
function rxcurrencyVnd(x, currency) {
  x = (Math.round(Number(x) * 100) / 100) || 0
  let symbol = 'AUD'
  let typecurrency = 'AUD'
  if (currency && currency.exchangerate) {
    let exchangerate = Number(currency.exchangerate.replace(/,/g,''))
    symbol = currency.symbol
    typecurrency = currency.name
    x = (typecurrency !== 'VND') ? (x * exchangerate).toFixed(2) : (x * exchangerate).toFixed(0)
  }
  let strresult = x.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ".")
  if (typecurrency === 'VND') {
    strresult = strresult + ' đ'
  } else if (typecurrency === 'AUD') {
    strresult = '$' + strresult + ' AUD'
  } else {
    strresult = symbol + ' ' + strresult 
  }
  return strresult
}
function validateEmail(email) {
  var re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
  return re.test(String(email).toLowerCase());
}
function rxgetdatetime (timestamp) {
  timestamp = (timestamp + 25200) * 1000
  let u = new Date(timestamp)
  let tempstr = 'Ngày ' + ('0' + u.getUTCDate()).slice(-2) + ' tháng ' + ('0' + (u.getUTCMonth() + 1)).slice(-2) + ' năm ' + u.getUTCFullYear()
  return tempstr
}
function helpCalDiscount(arrdiscount, priceold) {
  let objmeta = {}
  let pricenew = priceold
  if (arrdiscount){
    objmeta = arrdiscount['metadiscount']
    if (objmeta['typepromotion'] == 1 ) {
      if (arrdiscount['uselimit'] == false) {
        if ((arrdiscount['amountdiscount'] - arrdiscount['amountdiscountused']) > 0 ) {
          pricenew = priceold - objmeta['discountvalue']
        } else {
          pricenew = priceold
        }
      } else {
        pricenew = priceold - objmeta['discountvalue']
      }
    } else if (objmeta['typepromotion'] == 2) {
      pricenew = priceold - (priceold * objmeta['discountvalue'] / 100) 
    } else if (objmeta['typepromotion'] ==  3) {
      pricenew = priceold - objmeta['discountvalue']
    }
  }
  return pricenew
}

function randomcode() {
  let timenow = Math.floor(Date.now() / 1000)+''
  var stringcode = timenow.substring(4) + Number(Math.floor(1000 + Math.random() * 9000))
  return stringcode
}

module.exports = controller


