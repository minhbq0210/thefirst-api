let path = require('path')
let util = require('util')
let fs   = require('fs')
// let sharp = require('sharp')
let sizeOf = require('image-size')
let request  = require('request')
let http     = require('http')
let rxdata   = {}
const { writeFile } = require('fs');
const { extension } = require('mime-types');
const { basename, extname, join } = require('path');
const sanitize = require('sanitize-filename');

function controller(res) { this.res = res; rxController.call(this, res); rxdata = res.data }
util.inherits(controller, rxController)

controller.prototype.POSTindex = async function () {
  let rxfile = rxdata.files.uploadFile
  let rxfile_new = Math.round(rxu.now('micro')) + rxu.genstr() + path.extname(rxfile.name)
  fs.rename(rxfile.path, path.join(global.baseup_img, rxfile_new), (err) => {
    rxdata.response({ success: 1, msg: 'From index', data: rxfile_new })
  })
}

controller.prototype.POSTdown = async function() { 
  let arrimg = rxdata.params
  for (let i in arrimg) {
    let dest = join('./upload/', 'image');
    let url = arrimg[i];
    request({ url, encoding: null }, (error, response, body) => {
      if (error || response.statusCode < 200 || response.statusCode >= 300) {
          console.error('Request failed!');
          return;
      }
      let fileName = join(dest, sanitize(basename(url)));
      if (!extname(fileName)) {
          let contentType = response.headers['content-type'];
          let ext = extension(contentType);
          if (ext) {
              fileName += `.${ext}`;
          } else {
              console.error('Cannot detect file extension!');
          }
      }
      writeFile(fileName, body, (err) => {
          if (err) {
              console.error(err);
              return;
          }
          console.log('done');
      });
    });
  }
}

async function download(uri, filename, callback){
  request.head(uri, function(err, res, body){ 
    if(err){ console.log(err)}   
    request(uri).pipe(fs.createWriteStream(filename)).on('close', callback);
  });
};

module.exports = controller