let util     = require('util')
let mongoose = require('mongoose')

let ProductModel    = require(global.baseapp + 'models/productModel.js')
let OrderModel     = require(global.baseapp + 'models/orderModel.js')
let DashboardModel = require(global.baseapp + 'models/dashboardModel.js')

let rxdata   = {}

function controller(res) { this.res = res; rxController.call(this, res); rxdata = res.data }
util.inherits(controller, rxController)

///////////
// G E T //
///////////
controller.prototype.index = async function() { 
  this.helpCalRevenueInDay() 
  let timenow = Math.floor(Date.now() / 1000)
  let datestart = formatdate(timenow)
  if (rxdata.params.datestart) { datestart = formatdate(rxdata.params.datestart) }
  
  let date = new Date(datestart * 1000)
  let yeartmp  =  date.getFullYear()
  let monthtmp = date.getMonth()
  let datetmp  = date.getDate()
  let day = date.getDay();
  
  let firstDayofThisMonth = (new Date(yeartmp, monthtmp, 1).getTime()) / 1000
  let lastDayofThisMonth = (new Date(yeartmp, monthtmp + 1, 0).getTime()) / 1000
  let firstDayofLastMonth = (new Date(yeartmp, monthtmp - 1, 1).getTime()) / 1000
  let lastDayofLastMonth = (new Date(yeartmp, monthtmp, 0).getTime()) / 1000
  let firstDayofThisWeek = ((new Date(yeartmp, monthtmp, datetmp + (day == 0?-6:1)-day )).getTime()) / 1000
  let lastDayofThisWeek  = ((new Date(yeartmp, monthtmp, datetmp + (day == 0?0:7)-day )).getTime()) / 1000
  
  let condlastmonth = [{ '$gte': ['$date', firstDayofLastMonth] } , { '$lte': ['$date', lastDayofLastMonth] }]
  let condthismonth = [{ '$gte': ['$date', firstDayofThisMonth] } , { '$lte': ['$date', lastDayofThisMonth] }]
  let condthisweek  = [{ '$gte': ['$date', firstDayofThisWeek] } , { '$lte': ['$date', lastDayofThisWeek] }]
  let condyesterday = [{ '$gte': ['$date', datestart - 86400] } , { '$lte': ['$date', datestart - 86400] }]
  let condtoday     = [{ '$gte': ['$date', datestart] } , { '$lte': ['$date', datestart] }]

  let arrparams = [{id: 60, cond: condlastmonth}, {id: 30, cond: condthismonth}, {id: 7, cond: condthisweek}, {id: 1, cond: condyesterday}, {id: 0, cond: condtoday}]
  let queryobj = {_id: null}
  arrparams.forEach((obj) => {
    queryobj['amountorder_'+obj.id] = { '$sum': {'$cond': [ {'$and': obj.cond} , '$amountorder', 0 ]} }
    queryobj['rev_'+obj.id] = { '$sum': {'$cond': [ {'$and': obj.cond} , '$rev', 0 ]} }
    queryobj['arrproduct_'+obj.id] = { '$push': {'$cond': [ {'$and': obj.cond} , '$arrproduct', [] ]} }
    queryobj['chart_'+obj.id] = { '$push': {'$cond': [ {'$and': obj.cond} , { rev: "$rev", date: "$date" }, {} ]} }
  })

  let [err, dbarr] = await rxu.to(DashboardModel.aggregate( 
    { '$group': queryobj},
    {'$addFields':{ 'arrproduct_60':{ '$reduce':{ 'input':'$arrproduct_60', 'initialValue':[], 'in':{ '$concatArrays' : ['$$value', '$$this'] } } } } },
    {'$addFields':{ 'arrproduct_30':{ '$reduce':{ 'input':'$arrproduct_30', 'initialValue':[], 'in':{ '$concatArrays' : ['$$value', '$$this'] } } } } },
    {'$addFields':{ 'arrproduct_7':{ '$reduce':{ 'input':'$arrproduct_7', 'initialValue':[], 'in':{ '$concatArrays' : ['$$value', '$$this'] } } } } },
    {'$addFields':{ 'arrproduct_1':{ '$reduce':{ 'input':'$arrproduct_1', 'initialValue':[], 'in':{ '$concatArrays' : ['$$value', '$$this'] } } } } },
    {'$addFields':{ 'arrproduct_0':{ '$reduce':{ 'input':'$arrproduct_0', 'initialValue':[], 'in':{ '$concatArrays' : ['$$value', '$$this'] } } } } },
    {'$project': {
      '_id':0, 
      'lastmonth': {'amount': '$amountorder_60', 'rev': '$rev_60' , 'chart' : '$chart_60', 'arrproduct': { $map: { input: '$arrproduct_60', as: 'arrproducttmp', in: { $arrayElemAt:[{$split: ['$$arrproducttmp' , '|']}, 0]} } } },
      'thismonth': {'amount': '$amountorder_30', 'rev': '$rev_30' , 'arrproduct': { $map: { input: '$arrproduct_30', as: 'arrproducttmp', in: { $arrayElemAt:[{$split: ['$$arrproducttmp' , '|']}, 0]} } }, 'chart' : '$chart_30' },
      'lastweek': {'amount': '$amountorder_7', 'rev': '$rev_7' , 'arrproduct': { $map: { input: '$arrproduct_7', as: 'arrproducttmp', in: { $arrayElemAt:[{$split: ['$$arrproducttmp' , '|']}, 0]} } }, 'chart' : '$chart_7' },
      'yesterday': {'amount': '$amountorder_1', 'rev': '$rev_1' , 'arrproduct': { $map: { input: '$arrproduct_1', as: 'arrproducttmp', in: { $arrayElemAt:[{$split: ['$$arrproducttmp' , '|']}, 0]} } }, 'chart' : '$chart_1' },
      'today': {'amount': '$amountorder_0', 'rev': '$rev_0' , 'arrproduct': { $map: { input: '$arrproduct_0', as: 'arrproducttmp', in: { $arrayElemAt:[{$split: ['$$arrproducttmp' , '|']}, 0]} } }, 'chart' : '$chart_0'},
      }
    }
  ))
  let arrproducttotal = []
  for (let key in dbarr[0]) {
    let arrtmp = dbarr[0][key]['arrproduct']
    arrproducttotal = arrproducttotal.concat(countDiscountArr(arrtmp)) 
    arrproducttotal = [...new Set([...arrproducttotal])]
  }
  let paramsproduct = {_id: arrproducttotal}
  let [errproduct, dbarrproduct] = await rxu.to(ProductModel.find(paramsproduct))

  err ? this.cbFailed() : this.cbSuccess({chart: dbarr, products: dbarrproduct})
}

controller.prototype.helpCalRevenueInDay = async function() { 
  let timestart = rxdata.params.timestart
  let timenow = Math.floor(Date.now() / 1000)
  let datestart = (timestart) ? formatdate(timestart) : formatdate(timenow)
  let dateend = formatdate(timenow) + 86399
  
  let [err, dbarr] = await rxu.to(OrderModel.aggregate( 
    { '$match': { created_at: { '$gte': datestart, '$lte': dateend } } },
    { '$group': { 
        _id: {'$subtract': ['$created_at', {'$mod': [{'$sum': ['$created_at', 25200] }, 86400]}]}, 
        count: { '$sum': 1 },
        rev: { '$sum': '$price' },
        arrproduct: { '$push': '$arrcart'}
      }
    },
    {'$addFields':{
      'arrproduct':{
        '$reduce':{
            'input':'$arrproduct',
            'initialValue':[],
            'in':{ '$concatArrays' : ['$$value', '$$this']   }
          }
        }
      }
    },
    {'$project': {'_id':0, 'date': '$_id', 'count': '$count', 'rev': 1, 'arrproduct' : 1}}
  ))
  
  if (dbarr && dbarr.length > 0) {
    dbarr.forEach(objTemp => {
      try {
        let tempObj = {}
        tempObj['date']         = objTemp['date'];
        tempObj['amountorder']  = objTemp['count'];
        tempObj['rev']          = objTemp['rev'];
        tempObj['arrproduct']   = objTemp['arrproduct'];  
        tempObj['created_at']   = Math.floor(Date.now() / 1000);
        tempObj['updated_at']   = Math.floor(Date.now() / 1000);   

        DashboardModel.findOneAndUpdate(
          { date : tempObj['date'] }, 
          { $set: tempObj}, 
          { new: true, upsert: true}, 
          (err, newObj) => { if(err || !newObj) { console.log('Cant create or update permission!')}}
        ) 
      } catch (e) {
        console.log('Error:', e.stack);
      }
    });
  }
}

function formatdate(timestamp) {
  let date = (timestamp - timestamp % (86400)) - 25200
  return date
}

function countDiscountArr (arr) {
  let arrtmp = {}
  let arrsort = arr.sort()
  let count = 1
  for(let i=0;i<arrsort.length;i++)
  {
    if(arrsort[i]==arrsort[i+1]) 
    { 
      count++;
      arrtmp[arrsort[i]] = count
    }
    else {count = 1}
  }
  let keysSorted = Object.keys(arrtmp).sort(function(a,b){return arrtmp[b]-arrtmp[a]})
  return keysSorted.slice(0, 5)
}

module.exports = controller