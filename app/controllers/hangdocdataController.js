let fs         = require('fs')
let url        = require('url')
let util       = require('util')
let path       = require('path')
let http       = require('http')
let https      = require('https')
let fetch      = require('node-fetch')
let request    = require('request')
let express    = require('express')
let mongoose   = require('mongoose')
let rxdata     = {}

let Page       = require(global.baseapp + 'models/pageModel.js')
let Option     = require(global.baseapp + 'models/optionsModel.js')
let Detail     = require(global.baseapp + 'models/shopeeProductdetailModel.js')
let Product    = require(global.baseapp + 'models/shopeeProductModel.js') 
let Collect    = require(global.baseapp + 'models/collectsModel.js') 
let Content    = require(global.baseapp + 'models/contentModel.js')
let Category   = require(global.baseapp + 'models/shopeeCategoryModel.js')
let Collection = require(global.baseapp + 'models/shopeeCollectionModel.js')
let Contentcat = require(global.baseapp + 'models/contentcateModel.js')


function controller(res) { this.res = res; rxController.call(this, res); rxdata = res.data }
util.inherits(controller, rxController)

let username = 'c96c6298cb3b57dfc68397664027495f'
let password = '688c2ba20a78d32b9ed053d350ce2796'

let auth = "Basic " + new Buffer(username + ":" + password).toString("base64")

///////////
// G E T //
///////////

controller.prototype.index = async function() {  
  var paged = 0
  var request = require('request'),
    url_product = 'https://hangdoctumy.myharavan.com/admin/products.json?page=',
    url_page    = 'https://hangdoctumy.myharavan.com/admin/products/count.json'

    // Get category
    getCategory()
    // getPage()
    // getIdBlogs()
    // getArticle()

    request({  url : url_page, headers : {  "Authorization" : auth }},async function (err, res, body) {
      body = JSON.parse(body) 
      paged = Math.floor(body['count']/50) + 1
      for (var page = 1; page <= paged; page++) {
        let url_single = url_product+page
        crawerProduct(url_single, auth)
      }
    }) 
    this.cbSuccess('Updated product')      
}

let crawerProduct = async function(url_single, auth) {
  request({ url : url_single, headers : { "Authorization" : auth }}, async function (err, res, body) {
	if(err) {}
	if(res.statusCode !== 200 ) {}
	let tempcategoryArr = []
	body = JSON.parse(body) 
	let productArr = body.products
	let x = []
	updateValueSize = [], updateValueColor = []
	      
	for (let i in productArr) {
	    
	    let perDataAdd = {}
	    perDataAdd['appobj']       = []
	    perDataAdd['appdistobj']   = []
	    perDataAdd['appdist']      = []
	    let [errCollect, collects] = await rxu.to(Collect.find({product_id: body.products[i]['id']})) 
	    for (var c = 0; c <= collects.length; c++) {
	        if (collects[c] !== undefined) {
	            let [errCat, arrCat] = await rxu.to(Collection.findOne({id: collects[c]['collection_id']})) 
	            if (arrCat !== null) { perDataAdd['appdist'].push(arrCat._id), perDataAdd['appobj'].push(arrCat._id) } 
	        
	        }
	    }
	        
	    let [err, dbarrProduct] = await rxu.to(Product.find({id: productArr[i]['id']})) 
	    if (dbarrProduct.length === 0 ){
	      try{
	        getImageProduct((productArr[i].images))
	        perDataAdd['img_landscape']= productArr[i]['images'][0]['filename']
	      }catch(e){
	        perDataAdd['img_landscape']= 'ico_app_default.png'
	      }

	      perDataAdd['options'] = []
	      for (let h in productArr[i]['variants']) {
	          var options = {}
	          for (let k in productArr[i]['options']) {
	              var tempValSize = {}, tempValColor = {}
	              let posi = productArr[i]['options'][k]['position']
	              let tempOptionName = productArr[i]['options'][k]['name'].toLowerCase()
	              options[tempOptionName]    = {}
	              options['key']      = h
	              options['amount']   = productArr[i]['variants'][h]['inventory_quantity']
	              options['data']     = productArr[i]['variants'][h]
	              options[tempOptionName]['type']  = tempOptionName
	              options[tempOptionName]['name']  = tempOptionName
	              options[tempOptionName]['value'] = productArr[i]['variants'][h]['option'+ posi] || ''
	              options[tempOptionName]['text']  = productArr[i]['variants'][h]['option' + posi] || ''

	              if (tempOptionName === 'color') {
	                tempValColor['key'] = productArr[i]['variants'][h]['option'+ posi] || ''
	                tempValColor['value'] = productArr[i]['variants'][h]['option'+ posi] || ''
	              }
	              if (tempOptionName === 'size') {
	                tempValSize['key'] = productArr[i]['variants'][h]['option'+ posi] || ''
	                tempValSize['value'] = productArr[i]['variants'][h]['option'+ posi] || ''
	              }
	          }

	          perDataAdd['options'].push(options)
	      }
	      if (updateValueColor.length === 0) {
	        updateValueColor.push(tempValColor)
	      } else {
	        var found = updateValueColor.some(function (el) {
	        return el.key === tempValColor['key']
	        })
	        if (!found) { updateValueColor.push(tempValColor)}
	      }

	      if (updateValueSize.length === 0) {
	        updateValueSize.push(tempValSize)
	      } else {
	        var found = updateValueSize.some(function (el) {
	        return el.key === tempValSize['key']
	        })
	        if (!found) { updateValueSize.push(tempValSize)}
	      }
	      
	      perDataAdd['image_list'] = []
	      for (let l in productArr[i]['images']) {
	          perDataAdd['image_list'].push(productArr[i]['images'][l]['filename'])
	      }
	      perDataAdd['img_detail'] =  perDataAdd['image_list']
	      perDataAdd['tags']         = []
	      if (productArr[i]['tags'] != null) {
	          productArr[i]['tags'].split(',').map((i, k) => {
	              tempT = {}
	              tempT['id'] = k
	              tempT['text'] = i
	              perDataAdd['tags'].push(tempT)
	          })
	      }
	      perDataAdd['is_active']    = 1
	      perDataAdd['is_deleted']   = 0
	      perDataAdd['__v']          = 0
	      perDataAdd['stock']        = 0
	      perDataAdd['rule_time']    = ''
	      perDataAdd['filter_type']  = ''
	      perDataAdd['seotitle']     = ''
	      perDataAdd['seometa']      = ''
	      perDataAdd['desc']         = ''
	      perDataAdd['content']      = productArr[i]['body_html']
	      perDataAdd['product_cate'] = productArr[i]['product_type']
	      perDataAdd['created_at']   = Math.round(new Date(productArr[i]['created_at']).getTime()/1000)
	      perDataAdd['img_portrait'] = productArr[i]['img_portrait']
	      perDataAdd['images']       = productArr[i]['images']
	      perDataAdd['name']         = productArr[i]['title']
	      perDataAdd['slug']         = productArr[i]['handle']
	      perDataAdd['id']           = productArr[i]['id']
	      perDataAdd['price']        = productArr[i]['variants'][0]['price']
	      perDataAdd['published_at'] = productArr[i]['published_at']
	      perDataAdd['attributes']   = productArr[i]['variants']
	      perDataAdd['vendor']       = productArr[i]['vendor']
	      perDataAdd['body_plain']   = productArr[i]['body_plain']
	      perDataAdd['template_suffix'] = productArr[i]['template_suffix']
	      perDataAdd['published_scope'] = productArr[i]['published_scope']
	      perDataAdd['only_hide_from_list'] = productArr[i]['only_hide_from_list']
	      tempcategoryArr.push(perDataAdd)
	    } else {}
	   
	    updateOption('color', updateValueColor)
	    updateOption('size', updateValueSize)

	  }
	  Product.collection.insert(tempcategoryArr,  {keepGoing: true, continueOnError: true}, function(err, result) {})
  })
}

let getCategory = async function() {  
  var request = require('request'), 
  	urls = "https://hangdoctumy.myharavan.com/admin/custom_collections.json"
  	
  	request({ url : urls,headers : {"Authorization" : auth }},async function (err, res, body) {
        if(err) {}
        if(res.statusCode !== 200 ) {}
        var tempcategoryArr = []
        body = JSON.parse(body) 
        for (var i in body.custom_collections) {
            let [err, dbCollect] = await rxu.to(Collection.find({id: body.custom_collections[i]['id']})) 
            if (dbCollect && dbCollect.length === 0 ){
                var perDataAdd = {}
                perDataAdd['is_active']  = 1
                perDataAdd['is_deleted'] = 0
                perDataAdd['id']         = body.custom_collections[i]['id']
                perDataAdd['icons']      = body.custom_collections[i]['image']
                perDataAdd['desc']       = body.custom_collections[i]['template_suffix']
                perDataAdd['name']       = body.custom_collections[i]['title']
                perDataAdd['slug']       = body.custom_collections[i]['handle']
                perDataAdd['updated_at'] = body.custom_collections[i]['updated_at']
                perDataAdd['created_at'] = Math.round(new Date(body.custom_collections[i]['updated_at']).getTime()/1000)
                perDataAdd['products_count'] = body.custom_collections[i]['products_count']
                tempcategoryArr.push(perDataAdd)
            } else {}
        }
    Collection.collection.insert(tempcategoryArr,  {keepGoing: true, continueOnError: true}, function(err, result) {})
  })
}

let getPage = async function() {
	let url = 'https://hangdoctumy.myharavan.com/admin/pages.json'
	request({ url : url, headers : {"Authorization" : auth }}, async function (err, res, body) {
        if(err) {}

        if(res.statusCode === 200 ) {
        	var tempcategoryArr = []
	        body = JSON.parse(body)
	        for (var p in body.pages) {
	            let [err, dbPage] = await rxu.to(Page.find({id: body.pages[p]['id']})) 
	            if (dbPage && dbPage.length === 0 ){
	                var perArticle = {}
	                perArticle['data']       = body.pages[p]
	                perArticle['is_hot']     = 0
	                perArticle['is_active']  = 1
	                perArticle['is_deleted'] = 0
	                perArticle['created_at']  = rxu.time()
	                perArticle['updated_at']  = rxu.time()

	                perArticle['seometa']     = ''
	                perArticle['seotitle']    = ''
	                perArticle['content']     = ''

	                perArticle['id']         = body.pages[p]['id']
	                perArticle['content']    = body.pages[p]['body_html']
	                perArticle['desc']       = body.pages[p]['body_html']
	                perArticle['name']       = body.pages[p]['title']
	                perArticle['slug']       = body.pages[p]['handle']

	                perArticle['img_landscape'] = 'ico_app_default.png'
					perArticle['img_portrait']  = 'ico_app_default.png'
					perArticle['img_detail'] = 'ico_app_default.png'
					
	                tempcategoryArr.push(perArticle)
	            } else {}
	        }
	    	Page.collection.insert(tempcategoryArr,  {keepGoing: true, continueOnError: true}, function(err, result) {})
		
       	}
    })
}

let getIdBlogs = async function() {
	let url = 'https://hangdoctumy.myharavan.com/admin/Article/GetDropdownlist'
	let headers = {'cookie': 'verify_trusted_login=w2PDJlrjHejMefIx3fyg4AbiUeIGbRONsqU6TZcVgqvslLUO5vNRhfPF+DYyWHcXRQVcsS2AJXaay/7nI7fhmg==; bhnid=ij3wleeoq1i3ktei3o5kc0xw; csrf_token=A0tN_lK-KrfEQNmLzloCL5FveSFkKDuS132hG5wV4vJIvNvh4TvnTx0gCi8xbdnsU1rL42k36E5H1Ugb-dC5wETZXas1; __hssrc=1; hubspotutk=f197b968af57f337c8bf492a7e96878c; _ga=GA1.3.1275053921.1529216727; _gid=GA1.3.1330906877.1529216727; _ab=1; _abv=1; _ga=GA1.2.1856781161.1529216701; _gid=GA1.2.420449898.1529216701; lastshop=mualaket; __hstc=192063989.f197b968af57f337c8bf492a7e96878c.1529292508308.1529305341782.1529314412811.5; mp_fab05b8a88bcbc6d39c29af3122075e4_mixpanel=%7B%22distinct_id%22%3A%20%221640c6b78651cf-04484c499acebf-17356953-fa000-1640c6b7866a36%22%2C%22%24initial_referrer%22%3A%20%22https%3A%2F%2Fwww.haravan.com%2F%22%2C%22%24initial_referring_domain%22%3A%20%22www.haravan.com%22%7D; _gat=1; __hssc=192063989.2.1529314412811'}
    request.post({headers: headers, url: url },async function(err, res, body){
    	console.log(res.statusCode)
	  if(res.statusCode === 200 ) {
	  	var tempCate = []
        body = JSON.parse(body)
        let dataCate = body.Data.BlogList
        for (var j in dataCate) {
        	let [err, dbContentCat] = await rxu.to(Contentcat.find({Id: dataCate[j]['Id']})) 
        	if (dbContentCat && dbContentCat.length === 0 ){
                var perDataAdd = {}
                perDataAdd =dataCate[j]
                perDataAdd['name'] = dataCate[j]['Name']
                perDataAdd['is_active']  = 1
                perDataAdd['is_deleted'] = 0
                tempCate.push(perDataAdd)
            } else {}
            Contentcat.collection.insert(tempCate,  {keepGoing: true, continueOnError: true}, function(err, result) {})
	  		
        }
      } else {}
	})
}

let getArticle = async function() {
	console.log(rxu.time())
	let [errContent, dbContentcate] = await rxu.to(Contentcat.find()) 
	console.log(dbContentcate.length)
	if (dbContentcate) {
		for (let c in dbContentcate) {
			let catid = dbContentcate[c]['Id']
			let cat_id = dbContentcate[c]['_id']
			let url = 'https://hangdoctumy.myharavan.com/admin/blogs/'+catid+'/articles.json'
			request({ url : url, headers : {"Authorization" : auth }}, async function (err, res, body) {
		        if(err) {}

		        if(res.statusCode === 200 ) {
		        	var tempArticle = []
			        body = JSON.parse(body)
			        let articles = body.articles
			        if(articles.length !== 0) {
			        	for (var d in articles) {
				            let [err, dbContent] = await rxu.to(Content.find({id: articles[d]['id']})) 
				            getImagecontent(articles[d]['image']['src'], articles[d]['handle'])
				            if (dbContent && dbContent.length === 0 ){

				                var perArticle = {}
				                perArticle['data']       = articles[d]
				                perArticle['is_hot']     = 0
				                perArticle['is_active']  = 1
				                perArticle['is_deleted'] = 0
				                perArticle['created_at']  = rxu.time()
				                perArticle['updated_at']  = rxu.time()

				                perArticle['seometa']     = articles[d]['title']
				                perArticle['seotitle']    = articles[d]['title']
				                perArticle['content']     = articles[d]['title']

				                perArticle['app']        = cat_id
				                perArticle['appobj']     = cat_id
				                perArticle['appdist']    = cat_id
				                perArticle['appdistobj'] = cat_id
				                perArticle['id']         = articles[d]['id']
				                
				                perArticle['content']    = articles[d]['body_html']
				                perArticle['desc']       = articles[d]['body_html']
				                perArticle['name']       = articles[d]['title']
				                perArticle['slug']       = articles[d]['handle']
				                perArticle['img_landscape'] =  articles[d]['handle'] || 'ico_app_default.png'
  								perArticle['img_portrait']  =  articles[d]['handle'] || 'ico_app_default.png'
  								perArticle['img_detail'] =  articles[d]['handle'] || 'ico_app_default.png'
  								
				              
				                tempArticle.push(perArticle)
				            } else {}
				        }
			        } else {}
			        
			    	Content.collection.insert(tempArticle,  {keepGoing: true, continueOnError: true}, function(err, result) {})
				
		        } else {}
		    })
    	}	
	} else {}	
}

function updateOption(name, value) {
  let query = { 'type': name }, update = { '$set': {'value': value }}
  Option.collection.update(query, update)
}

function getImagecontent(images, name) {
  var srcpath = 'upload/image/'
  var data = images
  var uriTemp = []
  var download = function(uri, filename, callback) {
      var req = https.request(uri, function(res) {
          var file = fs.createWriteStream(srcpath + filename)
            res.pipe(file)
          })
          req.on('error', function(e) {
            console.log('error: ' + e.message)
          })
      req.end()    
  }
    var img_src = name 
    var filepath = images.replace('http', 'https')
    download(filepath, img_src, function() {
      console.log('downloaded', img_src, j)
    })
  var error = function(message) {
    console.log(message)
  }
}

function getImageProduct(images) {
  var srcpath = 'upload/image/'
  var data = images
  var uriTemp = []
  var download = function(uri, filename, callback) {
      var req = https.request(uri, function(res) {
          var file = fs.createWriteStream(srcpath + filename)
            res.pipe(file)
          })
          req.on('error', function(e) {
            console.log('error: ' + e.message)
          })
      req.end()    
  }
  for (var j in data) {
    var img_src = images[j]['filename'] 
    var filepath = images[j]['src'].replace('http', 'https')
    download(filepath, img_src, function() {
      console.log('downloaded', img_src, j)
    })
  }
  var error = function(message) {
    console.log(message)
  }
}

controller.prototype.getCollection = async function() { 
  let paged = 0
  let request = require('request'),
    url_collect = 'https://hangdoctumy.myharavan.com/admin/collects.json?page=',
    url_col_count = 'https://hangdoctumy.myharavan.com/admin/collects/count.json'
  request({url : url_col_count,headers : {  "Authorization" : auth }}, async function (err, res, page) {
	page = JSON.parse(page) 
	paged = Math.floor(page['count']/50) + 1
	console.log(paged)
	for (var page = 1; page <= paged; page++) {
	    request({url : url_collect + page,headers : {"Authorization" : auth }},async function (err, res, body) {
            if(err) {}
            if(res.statusCode !== 200 ) {}
            var tempcategoryArr = []
            body = JSON.parse(body) 
            for (var i in body.collects) {
                let [err, dbarrProduct] = await rxu.to(Collect.find({id: body.collects[i]['id']})) 
                if (dbarrProduct.length === 0 ){
                    var perDataAdd = {}
                    perDataAdd['id']            = body.collects[i]['id']
                    perDataAdd['position']      = body.collects[i]['position']
                    perDataAdd['product_id']    = body.collects[i]['product_id']
                    perDataAdd['sort_value']    = body.collects[i]['sort_value']
                    perDataAdd['collection_id'] = body.collects[i]['collection_id']
                    perDataAdd['is_active']  = 1
                    perDataAdd['is_deleted'] = 0
                    perDataAdd['updated_at'] = body.collects[i]['updated_at']
                    perDataAdd['created_at'] = Math.round(new Date(body.collects[i]['updated_at']).getTime()/1000)
                    tempcategoryArr.push(perDataAdd)
                }
            }
        // Collect.collection.insert(tempcategoryArr,  {keepGoing: true, continueOnError: true}, function(err, result) {})
	    })
	}
  })
}

controller.prototype.getCategory = async function() {		
  	this.cbSuccess({success: 'Success !', data: dbCollects}) 
}





module.exports = controller