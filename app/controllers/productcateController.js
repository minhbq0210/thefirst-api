let util     = require('util')
let mongoose = require('mongoose')
let Model    = require(global.baseapp + 'models/productcateModel.js')
let ModelOption    = require(global.baseapp + 'models/optionsModel.js')
let rxdata   = {}

function controller(res) { this.res = res; rxController.call(this, res); rxdata = res.data }
util.inherits(controller, rxController)

///////////
// G E T //
///////////
controller.prototype.index = async function() {
  let [err, dbarr] = await rxu.to(this.paging(this.filter(Model, rxdata.params), rxdata.params).find({is_brand: 0, type: {$ne: 1}}).exec()) //, type: 0
  let [err1, countCate] = await rxu.to(this.filter(Model, rxdata.params).find({ is_brand: 0, type: {$ne: 1}}).count().exec()) //, type: 0
  err ? this.cbFailed() : this.cbSuccess({ cate: dbarr, count: countCate })
}
controller.prototype.brand = async function() {
  let [err, dbarr] = await rxu.to(this.paging(this.filter(Model, rxdata.params), rxdata.params).find({is_brand: 1, type: {$ne: 1}}).exec()) //, type: 0
  let [err1, countBrand] = await rxu.to(this.filter(Model, rxdata.params).find({ is_brand: 1, type: {$ne: 1}}).count().exec()) //, type: 0
  err ? this.cbFailed() : this.cbSuccess({ brand: dbarr, count: countBrand })
}

controller.prototype.option_all = async function() {
  let [err, dbarr] = await rxu.to(this.paging(this.filter(Model, rxdata.params), rxdata.params).find({is_deleted: 0, is_active:1, type: {$ne: 1}}).exec())
  let [err1, countCate] = await rxu.to(this.filter(Model, rxdata.params).find({is_deleted: 0, is_active:1}).count().exec())
  err ? this.cbFailed() : this.cbSuccess({cates: dbarr, totals: countCate})
}

controller.prototype.collectioncate = async function() {
  let [err, dbarr] = await rxu.to(this.paging(this.filter(Model, rxdata.params), rxdata.params).find({type: 1, child: 0}).exec())
  let [err1, countBrand] = await rxu.to(this.filter(Model, rxdata.params).find({ type: 1, child: 0}).count().exec())
  err ? this.cbFailed() : this.cbSuccess({ cate: dbarr, count: countBrand })
}
controller.prototype.collection = async function() {
  let [err, dbarr] = await rxu.to(this.paging(this.filter(Model, rxdata.params), rxdata.params).find({type: 1, child: 1}).exec())
  let [err1, countBrand] = await rxu.to(this.filter(Model, rxdata.params).find({ type: 1, child: 1}).count().exec())
  err ? this.cbFailed() : this.cbSuccess({ cate: dbarr, count: countBrand })
}

controller.prototype.advertisementcate = async function() {
  let [err, dbarr] = await rxu.to(this.paging(this.filter(Model, rxdata.params), rxdata.params).find({type: 2, child: 0}).exec())
  let [err1, countBrand] = await rxu.to(this.filter(Model, rxdata.params).find({ type: 2, child: 0}).count().exec())
  err ? this.cbFailed() : this.cbSuccess({ cate: dbarr, count: countBrand })
}
controller.prototype.advertisement = async function() {
  let [err, dbarr] = await rxu.to(this.paging(this.filter(Model, rxdata.params), rxdata.params).find({type: 2, child: 1}).exec())
  let [err1, countBrand] = await rxu.to(this.filter(Model, rxdata.params).find({ type: 2, child: 1}).count().exec())
  err ? this.cbFailed() : this.cbSuccess({ cate: dbarr, count: countBrand })
}

controller.prototype.option = async function() {  
  // let [err, dbarrOptions] = await rxu.to(ModelOption.findOne({type: rxdata.params['type']}))      
  // err ? this.cbFailed() : this.cbSuccess({options: dbarrOptions})
  let [err, dbarr] = await rxu.to(this.paging(this.filter(Model, rxdata.params), rxdata.params).find({ $or: [ { is_brand: 0 }, { type: 1, child: 0 } ] }).exec())
  let [err1, countBrand] = await rxu.to(this.filter(Model, rxdata.params).find({ $or: [ { is_brand: 0 }, { type: 1, child: 0 } ] }).count().exec())
  err ? this.cbFailed() : this.cbSuccess({ cate: dbarr, count: countBrand })
}

controller.prototype.delete = async function() {  
  let [err, dbobj] = await rxu.to(Model.findByIdAndUpdate(rxdata.params['_id'], {is_deleted: 1}, {new: false}))
  err ? this.cbFailed() : this.cbSuccess(rxdata.params['_id'])
}

controller.prototype.restore = async function() {  
  let [err, dbobj] = await rxu.to(Model.findByIdAndUpdate(rxdata.params['_id'], {is_deleted: 0}, {new: false}))
  err ? this.cbFailed() : this.cbSuccess(rxdata.params['_id'])
}
controller.prototype.cate_brand = async function() {
  let [err1, dbarrCateAll] = await rxu.to(Model.find({is_deleted: 0}).sort({ 'priority': -1 }))
  let childArrCate = {}
  for (let key in dbarrCateAll) {
    let cateid = dbarrCateAll[key]['parent_id'] || 0
    if (!childArrCate[cateid]) {childArrCate[cateid] = []}
    childArrCate[cateid].push(dbarrCateAll[key])
  }
  let [err2, dbarrCateOnly] = await rxu.to(Model.find({is_deleted: 0, is_brand: 0}).sort({ 'priority': -1 }))
  let [err3, dbarrBrandOnly] = await rxu.to(Model.find({is_deleted: 0, is_brand: 1, type: {$ne: 1}}).sort({ 'priority': -1 })) //, type: 0
  let [err4, dbarrBrand] = await rxu.to(Model.find({is_deleted: 0, is_brand: 1, type: {$ne: 1}})) //, type: 0
  let childArrBrand = {}
  for(let key in dbarrBrand){
    let brand = dbarrBrand[key]
    if (brand.appdist && brand.appdist.length > 0) {
      if(brand.appdist !== undefined){
        let arraycat = brand.appdist.map((item) => mongoose.Types.ObjectId(item))
        let [err4, dbarr3] = await rxu.to(Model.find({'_id': {'$in' : arraycat}}))
        if(dbarr3){
          for(let i in dbarr3){
            let cateid = dbarr3[i]._id
            if (!childArrBrand[cateid]) {childArrBrand[cateid] = []}
            childArrBrand[cateid].push(brand)
          }
        }
      }
    } 
  }
  err1 ? this.cbFailed() : this.cbSuccess({childArrCate: childArrCate, dbarrCateOnly: dbarrCateOnly, childArrBrand: childArrBrand, dbarrBrandOnly: dbarrBrandOnly})
}
controller.prototype.cate_all = async function() {
  let [err, dbarr] = await rxu.to(Model.find({is_deleted: 0, type: {$ne: 1}}).sort({ 'priority': -1 })) //, type: 0
  let childArr = {}
  for (let key in dbarr) {
    let cateid = dbarr[key]['parent_id'] || 0
    if (!childArr[cateid]) {childArr[cateid] = []}
    childArr[cateid].push(dbarr[key])
  }
  err ? this.cbFailed() : this.cbSuccess(childArr)
}
controller.prototype.cate_only = async function() {
  let [err, dbarr] = await rxu.to(Model.find({is_deleted: 0, is_brand: 0, type: {$ne: 1}}).sort({ 'priority': -1 })) //, type: 0
  err ? this.cbFailed() : this.cbSuccess(dbarr)
}
controller.prototype.brand_all = async function() {
  let [err1, dbarr1] = await rxu.to(Model.find({is_deleted: 0, is_brand: 1, type: {$ne: 1}})) //, type: 0
  let childArr = {}
  for(let key in dbarr1){
    let brand = dbarr1[key]
    if (brand.appdist && brand.appdist.length > 0) {
      if(brand.appdist !== undefined){
        let arraycat = brand.appdist.map((item) => mongoose.Types.ObjectId(item))
        let [err3, dbarr3] = await rxu.to(Model.find({'_id': {'$in' : arraycat}}))
        if(dbarr3){
          for(let i in dbarr3){
            let cateid = dbarr3[i]._id
            if (!childArr[cateid]) {childArr[cateid] = []}
            childArr[cateid].push(brand)
          }
        }
      }
    } 
  }
  this.cbSuccess(childArr)
}

/////////////
// P O S T //
/////////////
controller.prototype.POSTindex = async function() {  
  let valid = this.validate({name: { required: true }})
  if (!valid) { this.cbFailed({ msg: 'Empty name' }); return false }

  rxdata.params.id = randomcard(10)
  rxdata.params.trace = (rxdata.params.trace) ? rxdata.params.trace +'-'+ rxdata.params.id : rxdata.params.id + ''
  rxdata.params.icons = null
  rxdata.params.desc  = rxdata.params.desc || 'collection'
  rxdata.params.slug  = rxChangeAlias(rxdata.params.name)
  rxdata.params.products_count = 0
  rxdata.params.created_at = Math.floor(Date.now() / 1000)
  rxdata.params.updated_at = Math.floor(Date.now() / 1000)

  let updating  = ['id', 'name', 'nameEng', 'parent_id', 'trace', 'icons', 'desc', 'slug', 'products_count', 'priority', 'is_active', 'is_deleted', 'updated_at', 'created_at','img_landscape', 'is_brand', 'appdist', 'tags', 'type', 'child']
  let modeldata = this.preUpdate(updating, rxdata.params) 
  modeldata.appdist = modeldata.appdist ? ((modeldata.appdist.constructor == Array)? modeldata.appdist : modeldata.appdist.split(',')) : null
  modeldata.type = 0
  
  // Check if this is valid _id
  for (let i in modeldata.appdist) {
    if (!mongoose.Types.ObjectId.isValid(modeldata.appdist[i])) {
      modeldata.appdist = null
      modeldata.appdistobj = null
      break
    }
  }
  for (let i in modeldata.brandlist) {
    if (!mongoose.Types.ObjectId.isValid(modeldata.brandlist[i])) {
      modeldata.brandlist = null
      break
    }
  }
  let dbObj = new Model(modeldata)
  
  let [err, dbarrObj] = await rxu.to(dbObj.save())
  err ? this.cbFailed() : this.cbSuccess(dbarrObj)
}

controller.prototype.POSTcollection = async function() {  
  let valid = this.validate({name: { required: true }})
  if (!valid) { this.cbFailed({ msg: 'Empty name' }); return false }

  rxdata.params.id = randomcard(10)
  rxdata.params.trace = (rxdata.params.trace) ? rxdata.params.trace +'-'+ rxdata.params.id : rxdata.params.id + ''
  rxdata.params.icons = null
  rxdata.params.desc  = rxdata.params.desc || 'collection'
  rxdata.params.slug  = rxChangeAlias(rxdata.params.name)
  rxdata.params.products_count = 0
  rxdata.params.created_at = Math.floor(Date.now() / 1000)
  rxdata.params.updated_at = Math.floor(Date.now() / 1000)

  let updating  = ['id', 'name', 'nameEng', 'parent_id', 'trace', 'icons', 'desc', 'slug', 'products_count', 'priority', 'is_active', 'is_deleted', 'updated_at', 'created_at','img_landscape', 'is_brand', 'appdist', 'tags', 'type', 'child']
  let modeldata = this.preUpdate(updating, rxdata.params) 
  modeldata.appdist = modeldata.appdist ? ((modeldata.appdist.constructor == Array)? modeldata.appdist : modeldata.appdist.split(',')) : null
  modeldata.type = 1
  modeldata.child = 1
  // Check if this is valid _id
  for (let i in rxdata.params.appdist) {
    if (!mongoose.Types.ObjectId.isValid(rxdata.params.appdist[i])) {
      rxdata.params.appdist = null
      rxdata.params.appdistobj = null
      break
    }
  }
  
  let dbObj = new Model(modeldata)
  
  let [err, dbarrObj] = await rxu.to(dbObj.save())
  err ? this.cbFailed() : this.cbSuccess(dbarrObj)
}

controller.prototype.POSTcollectioncate = async function() {  
  let valid = this.validate({name: { required: true }})
  if (!valid) { this.cbFailed({ msg: 'Empty name' }); return false }

  rxdata.params.id = randomcard(10)
  rxdata.params.trace = (rxdata.params.trace) ? rxdata.params.trace +'-'+ rxdata.params.id : rxdata.params.id + ''
  rxdata.params.icons = null
  rxdata.params.desc  = rxdata.params.desc || 'collection'
  rxdata.params.slug  = rxChangeAlias(rxdata.params.name)
  rxdata.params.products_count = 0
  rxdata.params.created_at = Math.floor(Date.now() / 1000)
  rxdata.params.updated_at = Math.floor(Date.now() / 1000)

  let updating  = ['id', 'name', 'nameEng', 'parent_id', 'trace', 'icons', 'desc', 'slug', 'products_count', 'priority', 'is_active', 'is_deleted', 'updated_at', 'created_at','img_landscape', 'is_brand', 'appdist', 'tags', 'type', 'child']
  let modeldata = this.preUpdate(updating, rxdata.params) 
  modeldata.appdist = modeldata.appdist ? ((modeldata.appdist.constructor == Array)? modeldata.appdist : modeldata.appdist.split(',')) : null
  modeldata.type = 1
  modeldata.child = 0
  
  let dbObj = new Model(modeldata)
  
  let [err, dbarrObj] = await rxu.to(dbObj.save())
  err ? this.cbFailed() : this.cbSuccess(dbarrObj)
}

controller.prototype.POSTadvertisement = async function() {  
  let valid = this.validate({name: { required: true }})
  if (!valid) { this.cbFailed({ msg: 'Empty name' }); return false }

  rxdata.params.id = randomcard(10)
  rxdata.params.trace = (rxdata.params.trace) ? rxdata.params.trace +'-'+ rxdata.params.id : rxdata.params.id + ''
  rxdata.params.icons = null
  rxdata.params.desc  = rxdata.params.desc || 'collection'
  rxdata.params.slug  = rxChangeAlias(rxdata.params.name)
  rxdata.params.products_count = 0
  rxdata.params.created_at = Math.floor(Date.now() / 1000)
  rxdata.params.updated_at = Math.floor(Date.now() / 1000)

  let updating  = ['id', 'name', 'nameEng', 'parent_id', 'trace', 'icons', 'desc', 'slug', 'products_count', 'priority', 'is_active', 'is_deleted', 'updated_at', 'created_at','img_landscape', 'is_brand', 'appdist', 'tags', 'type', 'child']
  let modeldata = this.preUpdate(updating, rxdata.params) 
  modeldata.appdist = modeldata.appdist ? ((modeldata.appdist.constructor == Array)? modeldata.appdist : modeldata.appdist.split(',')) : null
  modeldata.type = 2
  modeldata.child = 1
  // Check if this is valid _id
  for (let i in rxdata.params.appdist) {
    if (!mongoose.Types.ObjectId.isValid(rxdata.params.appdist[i])) {
      rxdata.params.appdist = null
      rxdata.params.appdistobj = null
      break
    }
  }
  
  let dbObj = new Model(modeldata)
  
  let [err, dbarrObj] = await rxu.to(dbObj.save())
  err ? this.cbFailed() : this.cbSuccess(dbarrObj)
}

controller.prototype.POSTadvertisementcate = async function() {  
  let valid = this.validate({name: { required: true }})
  if (!valid) { this.cbFailed({ msg: 'Empty name' }); return false }

  rxdata.params.id = randomcard(10)
  rxdata.params.trace = (rxdata.params.trace) ? rxdata.params.trace +'-'+ rxdata.params.id : rxdata.params.id + ''
  rxdata.params.icons = null
  rxdata.params.desc  = rxdata.params.desc || 'collection'
  rxdata.params.slug  = rxChangeAlias(rxdata.params.name)
  rxdata.params.products_count = 0
  rxdata.params.created_at = Math.floor(Date.now() / 1000)
  rxdata.params.updated_at = Math.floor(Date.now() / 1000)

  let updating  = ['id', 'name', 'nameEng', 'parent_id', 'trace', 'icons', 'desc', 'slug', 'products_count', 'priority', 'is_active', 'is_deleted', 'updated_at', 'created_at','img_landscape', 'is_brand', 'appdist', 'tags', 'type', 'child']
  let modeldata = this.preUpdate(updating, rxdata.params) 
  modeldata.appdist = modeldata.appdist ? ((modeldata.appdist.constructor == Array)? modeldata.appdist : modeldata.appdist.split(',')) : null
  modeldata.type = 2
  modeldata.child = 0
  
  let dbObj = new Model(modeldata)
  
  let [err, dbarrObj] = await rxu.to(dbObj.save())
  err ? this.cbFailed() : this.cbSuccess(dbarrObj)
}

controller.prototype.POSTedit = async function() {  
  this.validate({name: { required: true }})
  rxdata.params.slug  = rxChangeAlias(rxdata.params.name)
  rxdata.params.products_count = 0
  rxdata.params.updated_at = Math.floor(Date.now() / 1000)
  rxdata.params.appdist = rxdata.params.appdist ? ((rxdata.params.appdist.constructor == Array)? rxdata.params.appdist : rxdata.params.appdist.split(',')) : null
  // rxdata.params.type = 0
  // Check if this is valid _id
  for (let i in rxdata.params.appdist) {
    if (!mongoose.Types.ObjectId.isValid(rxdata.params.appdist[i])) {
      rxdata.params.appdist = null
      rxdata.params.appdistobj = null
      break
    }
  }
  for (let i in rxdata.params.brandlist) {
    if (!mongoose.Types.ObjectId.isValid(rxdata.params.brandlist[i])) {
      rxdata.params.brandlist = null
      break
    }
  }

  let updating  = ['name', 'nameEng', 'parent_id', 'trace', 'icons', 'desc', 'slug', 'products_count', 'priority', 'is_active', 'is_deleted', 'updated_at', 'img_landscape', 'is_brand', 'appdist', 'tags', 'type']

  let [err, dbobj] = await rxu.to(Model.findByIdAndUpdate(rxdata.params['_id'], this.preUpdate(updating, rxdata.params), {new: false}))
  err ? this.cbFailed() : this.cbSuccess(dbobj)
}

function randomcard(strleng) {
  return Number('10'+(Math.random().toString().slice(2,strleng)).toString())
}

function rxChangeAlias(x) {
  let str = x;
  str = str.toLowerCase();
  str = str.replace(/à|á|ạ|ả|ã|â|ầ|ấ|ậ|ẩ|ẫ|ă|ằ|ắ|ặ|ẳ|ẵ/g,'a'); 
  str = str.replace(/è|é|ẹ|ẻ|ẽ|ê|ề|ế|ệ|ể|ễ/g,'e'); 
  str = str.replace(/ì|í|ị|ỉ|ĩ/g,'i'); 
  str = str.replace(/ò|ó|ọ|ỏ|õ|ô|ồ|ố|ộ|ổ|ỗ|ơ|ờ|ớ|ợ|ở|ỡ/g,'o'); 
  str = str.replace(/ù|ú|ụ|ủ|ũ|ư|ừ|ứ|ự|ử|ữ/g,'u'); 
  str = str.replace(/ỳ|ý|ỵ|ỷ|ỹ/g,'y'); 
  str = str.replace(/đ/g,'d');
  str = str.replace(/ + /g,' ');
  str = str.replace(/ /g,'-');
  str = str.trim(); 
  return str; 
}

module.exports = controller