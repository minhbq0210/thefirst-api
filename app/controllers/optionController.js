let util     = require('util')
let mongoose = require('mongoose')
let Model    = require(global.baseapp + 'models/optionsModel.js')
let rxdata   = {}

function controller(res) { this.res = res; rxController.call(this, res); rxdata = res.data }
util.inherits(controller, rxController)

///////////
// G E T //
///////////
controller.prototype.index = async function() {   
  rxdata.params.pg_size = 100   
  let [err, dbarr] = await rxu.to(this.paging(this.filter(Model, rxdata.params), rxdata.params).exec())
  err ? this.cbFailed() : this.cbSuccess(dbarr)
}

controller.prototype.delete = async function() {  
  let [err, dbarr] = await rxu.to(Model.findByIdAndUpdate(rxdata.params['_id'], {is_deleted: 1}, {new: false}))
  err ? this.cbFailed() : this.cbSuccess(dbarr)
}

controller.prototype.restore = async function() {  
  let [err, dbarr] = await rxu.to(Model.findByIdAndUpdate(rxdata.params['_id'], {is_deleted: 0}, {new: false}))
  err ? this.cbFailed() : this.cbSuccess(rxdata.params['_id'])
}

/////////////
// P O S T //
/////////////
controller.prototype.POSTindex = async function() {  
  let updating  = [ 'name', 'key', 'type', 'value', 'is_active', 'is_hot' ]

  let modeldata = this.preUpdate(updating, rxdata.params)  
  let dbObj = new Model(modeldata)
  let [err, redbObj] = await rxu.to(dbObj.save())
  err ? this.cbFailed() : this.cbSuccess(redbObj)
}

controller.prototype.POSTedit = async function() {  
  let updating  = [ 'name', 'key', 'type', 'value', 'is_active', 'is_hot' ]
  let modeldata = this.preUpdate(updating, rxdata.params)  
  let [err, dbObj] = await rxu.to(Model.findByIdAndUpdate(rxdata.params['_id'], modeldata, {new: false}))
  err ? this.cbFailed() : this.cbSuccess(rxdata.params['_id'])
}

module.exports = controller