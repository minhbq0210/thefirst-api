let util     = require('util')
let mongoose = require('mongoose')
let Model    = require(global.baseapp + 'models/discountModel.js')
let rxdata   = {}

function controller(res) { this.res = res; rxController.call(this, res); rxdata = res.data }
util.inherits(controller, rxController)

///////////
// G E T //
///////////
controller.prototype.index = async function() {  
  let [err, dbarr] = await rxu.to(this.paging(this.filter(Model, rxdata.params), rxdata.params).exec())
  err ? this.cbFailed() : this.cbSuccess(dbarr)
}
controller.prototype.find = async function() { 
  let [err, dbarr] = await rxu.to(Model.findOne({code: rxdata.params['search_code'], is_deleted:0}))
  err ? this.cbFailed() : this.cbSuccess(dbarr)
}

controller.prototype.delete = async function() {  
  let [err, dbobj] = await rxu.to(Model.findByIdAndUpdate(rxdata.params['_id'], {is_deleted: 1}, {new: false}))
  err ? rxdata.response({msg: 'Cant delete'}) : this.cbSuccess(rxdata.params['_id'])
}

controller.prototype.restore = async function() {  
  let [err, dbobj] = await rxu.to(Model.findByIdAndUpdate(rxdata.params['_id'], {is_deleted: 0}, {new: false}))
  err ? rxdata.response({msg: 'Cant restore'}) : this.cbSuccess(rxdata.params['_id'])
}

controller.prototype.active = async function() {  
  let [err, dbobj] = await rxu.to(Model.findByIdAndUpdate(rxdata.params['_id'], {is_active: 1}, {new: false}))
  err ? rxdata.response({msg: 'Cant active'}) : this.cbSuccess(rxdata.params['_id'])
}

controller.prototype.inactive = async function() {  
  let [err, dbobj] = await rxu.to(Model.findByIdAndUpdate(rxdata.params['_id'], {is_active: 0}, {new: false}))
  err ? rxdata.response({msg: 'Cant inactive'}) : this.cbSuccess(rxdata.params['_id'])
}

/////////////
// P O S T //
/////////////
controller.prototype.POSTindex = async function() {    
  rxdata.params['amountdiscountused'] = 0
  let updating  = ['name', 'code', 'amountdiscount', 'programpromotion', 'metadiscount', 'startdate', 'enddate', 'useboth', 'uselimit']
  let modeldata = this.preUpdate(updating, rxdata.params) 
  modeldata.startdate = Math.floor(new Date(modeldata.startdate).getTime() + 86400)
  modeldata.enddate = Math.floor(new Date(modeldata.enddate).getTime() + 86400)
  let dbObj = new Model(modeldata)
  let [err, dbarr] = await rxu.to(dbObj.save())
  err ? this.cbFailed() : this.cbSuccess(dbarr)
}

controller.prototype.POSTedit = async function() { 
  let updating = ['name', 'code', 'amountdiscount', 'programpromotion', 'metadiscount', 'startdate', 'enddate', 'useboth', 'uselimit']
  let modeldata = this.preUpdate(updating, rxdata.params)
  modeldata.startdate = Math.floor(new Date(modeldata.startdate).getTime() + 86400)
  modeldata.enddate = Math.floor(new Date(modeldata.enddate).getTime() + 86400)
  let [err, dbObj] = await rxu.to(Model.findByIdAndUpdate(rxdata.params['_id'], modeldata, {new: false}))
  err ? this.cbFailed() : this.cbSuccess(rxdata.params['_id'])
}

module.exports = controller