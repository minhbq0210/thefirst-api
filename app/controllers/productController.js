let util     = require('util')
let mongoose = require('mongoose')
let Model    = require(global.baseapp + 'models/productModel.js')
let User     = require(global.baseapp + 'models/userModel.js')
let rxdata   = {}
let request  = require('request')
const { writeFile } = require('fs');
const { extension } = require('mime-types');
const { basename, extname, join } = require('path');
const sanitize = require('sanitize-filename');

function controller(res) { this.res = res; rxController.call(this, res); rxdata = res.data }
util.inherits(controller, rxController)

///////////
// G E T //
///////////
controller.prototype.index = async function() {  
  let [err, dbarr] = await rxu.to(this.paging(this.filter(Model, rxdata.params), rxdata.params).populate('appdistobj appobj').exec())
  err ? this.cbFailed() : this.cbSuccess(dbarr)
}
controller.prototype.getSale = async function() { 
  let [err, dbarr] = await rxu.to(this.paging(this.filter(Model, rxdata.params).find({is_deleted: 0, is_active:1}), rxdata.params).populate('appdistobj appobj').exec())
  let [err1, countProduct] = await rxu.to(this.filter(Model, rxdata.params).find({is_deleted: 0, is_active:1}).count().exec())
  err ? this.cbFailed() : this.cbSuccess({product: dbarr, totals: countProduct})
}

controller.prototype.delete = async function() {  
  let [err, dbarr] = await rxu.to(Model.findByIdAndUpdate(rxdata.params['_id'], {is_deleted: 1}, {new: false}))
  err ? this.cbFailed() : this.cbSuccess(rxdata.params['_id'])
}

controller.prototype.restore = async function() {  
  let [err, dbarr] = await rxu.to(Model.findByIdAndUpdate(rxdata.params['_id'], {is_deleted: 0}, {new: false}))
  err ? this.cbFailed() : this.cbSuccess(rxdata.params['_id'])
}

controller.prototype.inventory = async function() {  
  let pg_page = (typeof(rxdata.params.pg_page) == 'undefined' || parseInt(rxdata.params.pg_page) < 1 )? 1  : parseInt(rxdata.params.pg_page)
  let pg_size = (typeof(rxdata.params.pg_size) == 'undefined' || parseInt(rxdata.params.pg_size) < 1 || parseInt(rxdata.params.pg_size) > 1000)? 10 : parseInt(rxdata.params.pg_size)
  // Sorting
  let st_col  = rxdata.params.st_col  || 'created_at'
  let st_type = (typeof(rxdata.params.st_type) == 'undefined' || parseInt(rxdata.params.st_type) != 1)? -1 : 1
  let st_params = {}; st_params[st_col] = st_type

  let params = {inventory_policy: 1, is_deleted: 0, is_active: 1}
  if (rxdata.params['search_name']) { params['name'] = new RegExp(rxdata.params['search_name'], "i")}
  let [err, dbarr] = await rxu.to(Model.aggregate( 
    {'$unwind': '$stock'}, 
    {'$match': params},
    {'$limit': ((pg_page) * pg_size) },
    {'$skip': ((pg_page - 1) * pg_size) },
    {'$sort': st_params }
  ))
  err ? this.cbFailed() : this.cbSuccess(dbarr)
}

/////////////
// P O S T //
/////////////
controller.prototype.POSTindex = async function() {
  if (this.validate({
    name: {required: true}, 
    price: {required: true},
    img_landscape: {required: true}
  })) {
    let updating  = ['name', 'nameEng', 'content', 'options','attributes', 'tags', 'slug','seotitle', 'seometa', 'desc', 'price', 'price_old','price_discount', 'stock','grams','sku','barcode','code','requires_shipping','inventory_adjustment', 'inventory_policy', 'appobj', 'filter_type', 'appdist', 'appdistobj', 'img_landscape', 'img_portrait', 'img_detail','image_list','vendor', 'rule_time', 'is_active', 'is_hot', 'brandlist']
    let modeldata = this.preUpdate(updating, rxdata.params) 
    modeldata.slug    = modeldata.name.replace(/[^-a-zA-Z0-9\s+]+/ig, '').replace(/\s+/gi, "-").replace('+', '-').toLowerCase();
    modeldata.seotitle = modeldata.seotitle || modeldata.name
    modeldata.seometa  = modeldata.seometa || modeldata.desc  
    modeldata.appdist = modeldata.appdist ? ((modeldata.appdist.constructor == Array)? modeldata.appdist : modeldata.appdist.split(',')) : null
    modeldata.appdistobj = modeldata.appdist  
    modeldata.brandlist = modeldata.brandlist ? ((modeldata.brandlist.constructor == Array)? modeldata.brandlist : modeldata.brandlist.split(',')) : null

    // Check if this is valid _id
    for (let i in modeldata.appdist) {
      if (!mongoose.Types.ObjectId.isValid(modeldata.appdist[i])) {
        modeldata.appdist = null
        modeldata.appdistobj = null
        break
      }
    }
    for (let i in modeldata.brandlist) {
      if (!mongoose.Types.ObjectId.isValid(modeldata.brandlist[i])) {
        modeldata.brandlist = null
        break
      }
    }

    let dbObj = new Model(modeldata)
    let [err, dbarr] = await rxu.to(dbObj.save())
    err ? this.cbFailed() : this.cbSuccess(dbarr)
  }
}

controller.prototype.updateprice = async function() {
  let currency = 16500
  let [err, dbarr] = await rxu.to(this.filter(Model).exec())
  // let updating = ['name','content','options','attributes', 'tags','slug','seotitle', 'seometa', 'desc', 'price', 'price_old','price_discount', 'stock','grams','sku','barcode','code','requires_shipping','inventory_adjustment', 'inventory_policy', 'appobj', 'filter_type', 'appdist', 'appdistobj', 'img_landscape', 'img_portrait', 'img_detail','image_list','vendor', 'rule_time', 'is_active', 'is_hot', 'brandlist']
  for(let i in dbarr){
    let price = (dbarr[i].price/currency).toFixed(2)
    // let price = (dbarr[i].price*currency)
    let price_old = 0
    if(dbarr[i].price_old){
      price_old = (dbarr[i].price_old/currency).toFixed(2)
      // price_old = (dbarr[i].price_old*currency)
    }
    else{
      price_old = price
    }
    dbarr[i].price = price
    dbarr[i].price_old = price_old
    let [err1, dbarr1] = await rxu.to(Model.findByIdAndUpdate(dbarr[i]._id, {price_old: price_old, price: price}, {new: false}))
  }
  this.cbSuccess()
}

controller.prototype.POSTedit = async function() {  
  let updating = ['name', 'nameEng', 'content','options','attributes', 'tags','slug','seotitle', 'seometa', 'desc', 'price', 'price_old','price_discount', 'stock','grams','sku','barcode','code','requires_shipping','inventory_adjustment', 'inventory_policy', 'appobj', 'filter_type', 'appdist', 'appdistobj', 'img_landscape', 'img_portrait', 'img_detail','image_list','vendor', 'rule_time', 'is_active', 'is_hot', 'brandlist']
  let modeldata = this.preUpdate(updating, rxdata.params)
  modeldata.slug   = modeldata.slug || modeldata.name.replace(/[^-a-zA-Z0-9\s+]+/ig, '').replace(/\s+/gi, "-").toLowerCase();
  modeldata.seotitle = modeldata.seotitle || modeldata.name
  modeldata.seometa  = modeldata.seometa || modeldata.desc  
  modeldata.appdist = modeldata.appdist ? ((modeldata.appdist.constructor == Array)? modeldata.appdist : modeldata.appdist.split(',')) : null
  modeldata.appdistobj = modeldata.appdist
  modeldata.brandlist = modeldata.brandlist ? ((modeldata.brandlist.constructor == Array)? modeldata.brandlist : modeldata.brandlist.split(',')) : null

  // Check if this is valid _id
  for (let i in modeldata.appdist) {
    if (!mongoose.Types.ObjectId.isValid(modeldata.appdist[i])) {
      modeldata.appdist = null
      modeldata.appdistobj = null
      break
    }
  }
  for (let i in modeldata.brandlist) {
    if (!mongoose.Types.ObjectId.isValid(modeldata.brandlist[i])) {
      modeldata.brandlist = null
      break
    }
  }

  let [err, dbarr] = await rxu.to(Model.findByIdAndUpdate(rxdata.params['_id'], modeldata, {new: false}))
  err ? this.cbFailed() : this.cbSuccess(rxdata.params['_id'])
}

controller.prototype.POSTrating = async function() {  
  rxdata.params['ratings']['created_at'] = Math.floor(Date.now() / 1000)
  let [errpull, dbarrpull] = await rxu.to(Model.findByIdAndUpdate({_id: rxdata.params['_id']},{ $pull: { 'ratings': { 'customerid': rxdata.params['ratings']['customerid'] }}}, {new: false}))
  let [errpush, dbarrpush] = await rxu.to(Model.findByIdAndUpdate({_id: rxdata.params['_id']},{$addToSet: { ratings: rxdata.params['ratings']}}, {new: true, upsert: true}))

  let arrayrating = dbarrpush.ratings.map((item) => item)
  let count = arrayrating.length
  let sumscore = 0
  for (var key in arrayrating) {
    var keycustomer = Object.keys(arrayrating[key])
    sumscore += arrayrating[key]['score']
  }
  let paramsupdate = (count) ? {'ratingsSumary' : Math.ceil(sumscore/count)} : {'ratingsSumary' : 0}
  let [errrating, dbrating] = await rxu.to(Model.findByIdAndUpdate(rxdata.params['_id'], paramsupdate, {new: false, upsert: true}))
  errrating ? this.cbFailed() : this.cbSuccess(rxdata.params['_id'])
}

controller.prototype.POSTinventory = async function() {  
  let [err, dbarr] = await rxu.to(Model.findOneAndUpdate({ '_id':mongoose.Types.ObjectId(rxdata.params['_id']), 'inventory_policy': 1, 'is_deleted': 0, options: { $elemMatch: { key: rxdata.params['key']} }}, { $set : { 'options.$.amount' : rxdata.params['amount']}},{ new: true, upsert: false, multi: true}))
  err ? this.cbFailed() : this.cbSuccess(dbarr)
}



module.exports = controller