// Base setup
//=======================================
let util       = require('util')
let fetch      = require('node-fetch');
let express    = require('express')
let request    = require('request')
let mongoose   = require('mongoose')
let http       = require('http')
let https      = require('https')
let path       = require('path')
let url        = require('url')
let fs         = require('fs')
  
let ProductShopee = require(global.baseapp + 'models/shopeeProductModel.js') 
let ProductDetail = require(global.baseapp + 'models/shopeeProductdetailModel.js')
let Category      = require(global.baseapp + 'models/shopeeCategoryModel.js')
let Collection    = require(global.baseapp + 'models/shopeeCollectionModel.js')

function controller(res) { this.res = res; rxController.call(this, res); global.rxdata = res.data }
util.inherits(controller, rxController)

var headers = {
                'accept-language': 'en-US,en;q=0.9,vi;q=0.8',
                'cache-control': 'no-cache',
                'cookie': '_ga=GA1.2.261558568.1526279425; SPC_IA=-1; SPC_F=Y0QIDhZC4GLdO2FWhh3WY7I5pAFRnaYM; REC_T_ID=4a7935bc-5740-11e8-9876-1866daa86183; SPC_EC="SGt7pXCEiD2Ze+NJxVUCOR7j9UZhK+QlPgYjJU7Eb+MH/VHKSfF6mcyidDGtx6PesBR3sBGwNzJvYQzViFqur68yyToxZMDAo3W/+/5FEuBAnQp3fsZHuu8PTyEySDOA6Od//VYIQVdoy+TrzKGioQ=="; SPC_T_ID="2o5q/Qjaz+zXimJ3969HVQKvqI88aEEo9qo9Tj7r9ij1r8Nc6Qikq/hnedIFrjoB1eybOKWKWk775NjLUhdMwEl2UFYA4zBzpJ9TG6tTJ0I="; SPC_U=63925305; SPC_T_IV="r1Eqd2e1s5XKJE994+ND1A=="; _gid=GA1.2.809114744.1526880790; SPC_SI=qnfo7l3amzurwbax14y3dlntswccf333; SPC_SC_TK=8a0c8c32e952b5cff93171bafe34a720; SPC_SC_UD=63925305; csrftoken=aWRzYZscpkwrV5vo500NIuxkx5xTkOgC; bannerShown=true',
                'if-none-match-': '55b03-98a2b54c90856b5a24eb37cf1705da99',
                'pragma': 'no-cache',
                'referer': 'https://shopee.vn/hang_nhap_my',
                'user-agent': 'Mozilla/5.0 (iPhone; CPU iPhone OS 11_0 like Mac OS X) AppleWebKit/604.1.38 (KHTML, like Gecko) Version/11.0 Mobile/15A372 Safari/604.1'
              }

///////////
// G E T //
///////////

controller.prototype.index = async function() {  
  var request = require('request'),
    url_shop = 'https://shopee.vn/api/v2/search_items/?by=relevancy&limit=20&match_id=2318754&order=desc&page_type=shop&newest='
    request({  url : url_shop + 0,headers : headers },
    function (err, res, body) {
      body = JSON.parse(body) 
      var paged = Math.floor(body['total_count']/20) + 1
      for (var page = 0; page <= paged; page++) {
        url_all = url_shop + page*20
        crawlerAllProduct(url_all)
      }
    })
}
// url_shop = 'https://shopee.vn/api/v2/search_items/?by=pop&limit=100&match_id=2318754&order=desc&page_type=shop&newest='
    
function crawlerAllProduct(url_all) {
  console.log(1)
  request({ url : url_all, headers : headers },
  async function (err, res, body) {
      if(err) {}
      if(res.statusCode !== 200 ) {}
      var itemsArr = JSON.parse(res.body)
      var tempArr = []
      for (var i in itemsArr.items) { 
        var [err, dbarrProduct] = await rxu.to(ProductShopee.find({itemid: itemsArr.items[i]['itemid']})) 
        if (dbarrProduct.length === 0 ){
          var itemid = itemsArr.items[i]['itemid']
          crawlerDetailProduct(itemid)
        }
      }
  })
}

function crawlerDetailProduct(itemid) {
  var request = require('request'),
    url_detail = 'https://shopee.vn/api/v2/item/get?shopid=2318754&itemid='  
    request({ url : url_detail + itemid, headers :headers },
      async function (err, res, body) {
        if(err) {}

        var itemsArr_ = JSON.parse(res.body)
        itemsArr_ = itemsArr_.item
        try{
          getImageProduct((itemsArr_.images))
        }catch(e){}

        itemsArr_['is_active']    = 1
        itemsArr_['is_deleted']   = 0
        itemsArr_['seotitle']     = ''
        itemsArr_['seometa']      = ''
        itemsArr_['desc']         = ''
        itemsArr_['img_portrait'] = itemsArr_['image']
        itemsArr_['img_landscape']= itemsArr_['image']
        itemsArr_['appobj']       = []
        itemsArr_['appdistobj']   = []
        itemsArr_['appdist']      = []
        itemsArr_['content']      = itemsArr_['description']
        itemsArr_['created_at']   = Math.round(new Date().getTime()/1000)
        itemsArr_['price']        = itemsArr_['price']/100000
        itemsArr_['slug']         = itemsArr_['name'].replace(/[^-a-zA-Z0-9\s+]+/ig, '').replace(/\s+/gi, "-").toLowerCase()
        itemsArr_['tags']         = []  
        if (itemsArr_['hashtag_list'] != null) {
            itemsArr_['hashtag_list'].map((i, k) => {
                tempT = {}
                tempT['id'] = k
                tempT['text'] = i
                itemsArr_['tags'].push(tempT)
            })
        }
        itemsArr_['options'] = []
        if (itemsArr_['models'].length !== 0){
            for (var i in itemsArr_['models']) {
                temp = {}
                itemsArr_['models'][i]['price'] = itemsArr_['models'][i]['price']/100000
                temp['amount'] = itemsArr_['models'][i]['stock']
                temp['key']    = i.toString()
                temp['name']   = {}
                temp['name']['type'] = 'name'
                temp['name']['name'] = 'Phân loại hàng'
                temp['name']['value']= itemsArr_['models'][i]['name']
                temp['name']['text'] = itemsArr_['models'][i]['name']
                temp['data']   = itemsArr_['models'][i]
                itemsArr_['options'].push(temp)
            }
        } else {
          temp = {}
          temp['key'] = '0'
          temp['amount'] = itemsArr_['stock']
          temp['data'] = []
          itemsArr_['options'].push(temp)
        }
        let [errCollect, collects] = await rxu.to(Collection.find({'$or': [{'shop_collection_id': itemsArr_['categories'][0]['catid']},{'shop_collection_id': itemsArr_['categories'][1]['catid']}, {'shop_collection_id': itemsArr_['categories'][2]['catid']}]  })) 
        for (var c = 0; c <= collects.length; c++) {
          if (collects[c] !== undefined) {
            itemsArr_['appdist'].push(collects[c]._id), itemsArr_['appobj'].push(collects[c]._id)
          }
        }
       ProductShopee.collection.insert(itemsArr_,  {keepGoing: true, continueOnError: true}, function(err, result) {})

    })
}

function getImageProduct(images) {
  var srcpath = 'upload/image_v2/',
  data = images,
  urlRoot = 'https://cf.shopee.vn/file/'
  var uriTemp = []
  var download = function(uri, filename, callback) {
      var req = https.request(uri, function(res) {
          var file = fs.createWriteStream(srcpath + filename)
            res.pipe(file)
          })
          req.on('error', function(e) {
            console.log('error: ' + e.message)
          })
      req.end()    
  }
  for (var j in data) {
    var img_src = data[j] 
    var filepath = data[j]
    filepath = urlRoot + filepath
    download(filepath, img_src, function() {
      console.log('downloaded', img_src, j)
    })
  }
  var error = function(message) {
    console.log(message)
  }
}

controller.prototype.getCategory = async function() {  
  var request = require('request'),
    url_catall = 'https://shopee.vn/api/v1/category_list/'
    request(
        {
            url : url_catall,
            headers : headers
        },
        async function (err, res, body) {
            if(err) {}
            if(res.statusCode !== 200 ) {}
                var itemsArr = JSON.parse(res.body)
                var tempArr = []
                for (var i in itemsArr) { 
                    itemsArr[i]['is_active']    = 1
                    itemsArr[i]['is_deleted']   = 0
                    itemsArr[i]['created_at']   = Math.round(new Date().getTime()/1000)
                    tempArr.push(itemsArr[i])
            }
            console.log(tempArr.length)
            
        Category.collection.insert(tempArr,  {keepGoing: true, continueOnError: true}, function(err, result) {})
    })
}

controller.prototype.getCollection = async function() {  
  var request = require('request'),
    url_collect = 'https://shopee.vn/api/v2/shop/get_categories?limit=20&offset=0&shopid=2318754'  
    request(
        {
            url : url_collect,
            headers : headers
        },
        async function (err, res, body) {
            if(err) {}
            if(res.statusCode !== 200 ) {}
                var itemsArr = JSON.parse(res.body)
                itemsArr = itemsArr.data.items
                var tempArr = []
                for (var i in itemsArr) { 
                    itemsArr[i]['is_active']    = 1
                    itemsArr[i]['is_deleted']   = 0
                    itemsArr[i]['created_at']   = Math.round(new Date().getTime()/1000)
                    tempArr.push(itemsArr[i])
            }
            console.log(tempArr.length)
            
        Collection.collection.insert(tempArr,  {keepGoing: true, continueOnError: true}, function(err, result) {})
    })
}

module.exports = controller