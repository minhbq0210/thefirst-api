let mongoose = require('mongoose')
let Schema = mongoose.Schema

// create a schema
let schema = new Schema({  
  name: { type: String, default: '' },
  code: { type: String, default: '' },

  desc: { type: String, default: '' },

  type: { type: Number, default: 0 },
  amountdiscount: { type: Number, default: 0 },
  amountdiscountused: {type: Number, default: 0},
  programpromotion: { type: Number, default: 0 },
  metadiscount: { type: Object, default: {} },

  startdate: { type: Number, default: 0 },
  enddate: { type: Number, default: 0 },
  useboth: { type: Boolean, default: false },
  uselimit: { type: Boolean, default: false },

  is_deleted: { type: Number, default: 0 },
  is_active:  { type: Number, default: 1 },
  created_at: { type: Number, default: 0 },
  updated_at: { type: Number, default: 0 },  
}, {collection: 'discount'})

schema.pre('save', function(next) {  
  this.created_at = Math.floor(Date.now() / 1000)
  this.updated_at = Math.floor(Date.now() / 1000)
  next()
});

let Model = mongoose.model('Discount', schema)
module.exports = Model