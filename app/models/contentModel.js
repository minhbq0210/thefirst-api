let mongoose = require('mongoose')
let Schema = mongoose.Schema
let Contentcat = require(global.baseapp + 'models/contentcateModel.js')
// create a schema
let schema = new Schema({  
  name: { type: String, default: '' },
  desc: { type: String, default: '' },
  content: { type: String, default: '' },
  price: { type: Number, default: 0 },
  stock: { type: Number, default: 100 },
  slug: { type: String, default: '' },

  seotitle: { type: String, default: '' },
  seometa: { type: String, default: '' },

  app: { type: mongoose.Schema.Types.ObjectId, ref: 'Contentcat'},
  appobj: { type: mongoose.Schema.Types.ObjectId, ref: 'Contentcat'},
  appdist: [{ type: mongoose.Schema.Types.ObjectId, ref: 'Contentcat'}],
  appdistobj: [{ type: mongoose.Schema.Types.ObjectId, ref: 'Contentcat'}],

  img_landscape: { type: String, default: '' },
  img_portrait: { type: String, default: '' },
  img_detail: { type: String, default: '' },

  is_deleted: { type: Number, default: 0 },
  is_active:  { type: Number, default: 1 },
  is_hot:     { type: Number, default: 0 },
  created_at: { type: Number, default: 0 },
  updated_at: { type: Number, default: 0 }, 

  ratings: {type: Array, default: []},
  ratingsSumary: {type: Number, default: 0}, 
}, {collection: 'content'})

schema.pre('save', function(next) {  
  this.created_at = Math.floor(Date.now() / 1000)
  this.updated_at = Math.floor(Date.now() / 1000)
  next()
});

let Model = mongoose.model('Content', schema)
module.exports = Model