let mongoose = require('mongoose')
let Schema = mongoose.Schema
// create a schema
let schema = new Schema({  
  rev: { type: Number, default: 0 },
  arrproduct: { type: Array, default: [] },
  date: { type: Number, default: 0},
  amountorder: { type: Number, default: 0 },
  is_deleted: { type: Number, default: 0 },
  is_active:  { type: Number, default: 1 },
  created_at: { type: Number, default: 0 },
  updated_at: { type: Number, default: 0 },  
}, {collection: 'dashboard'})

schema.pre('save', function(next) {  
  this.created_at = Math.floor(Date.now() / 1000)
  this.updated_at = Math.floor(Date.now() / 1000)
  next()
});

let Dashboard = mongoose.model('Dashboard', schema)
module.exports = Dashboard