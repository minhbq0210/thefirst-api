let mongoose = require('mongoose')
let Schema = mongoose.Schema
let Productcate = require(global.baseapp + 'models/productcateModel.js')
// create a schema
let schema = new Schema({  
  name: { type: String, default: '' },
  email: { type: String, default: '' },
  phone: { type: String, default: '' },
  address: { type: Array, default: [] },
  detail: { type: String, default: '' },
  desc: { type: String, default: '' },
  price: { type: Number, default: 0 },
  order_history: {type: String, default: ''},
  order_note: {type: String, default: ''},
  order_code: {type: String, default: ''},
  status_confirm: { type: Number, default: 0 },
  status_ship: { type: Number, default: 0 },
  status_order_payment: { type: Number, default: 0 },
  status_COD: { type: Number, default: 0 },
  type_channel_name: { type: String, default: 'web' },
  type_payment: { type: Number, default: 0 },
  history_order: { type: Array, default: [] },
  arrcart: { type: Array, default: [] },
  status_order: { type: Number, default: 0 },
  is_deleted: { type: Number, default: 0 },
  is_active:  { type: Number, default: 1 },
  created_at: { type: Number, default: 0 },
  updated_at: { type: Number, default: 0 },
  totalweight: { type: Number, default: 0 },
  currency: { type: Object, default: {}},
  reason_deleted: { type: String, default: ''},
  
  fee_ship: { type: Number, default: 0 },
  price_discount: { type: Number, default: 0 },
  totalpay: { type: Number, default: 0 },
  discount_bill: { type: Number, default: 0 },  
}, {collection: 'order'})


schema.pre('save', function(next) {  
  this.created_at = Math.floor(Date.now() / 1000)
  this.updated_at = Math.floor(Date.now() / 1000)
  next()
});

let Order = mongoose.model('Order', schema)
module.exports = Order