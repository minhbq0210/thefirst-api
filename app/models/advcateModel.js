let mongoose = require('mongoose')
let Schema = mongoose.Schema
let Productcate = require(global.baseapp + 'models/productcateModel.js')

// create a schema
let schema = new Schema({  
  name: { type: String, default: '' },
  desc: { type: String, default: '' },
  content: { type: String, default: '' },
  slug: { type: String, default: '' },

  seotitle: { type: String, default: '' },
  seometa: { type: String, default: '' },

  img_landscape: { type: String, default: '' },
  icons:      { type: String, default: ''},  
  position: { type: Number, default: 1 },

  childArr: { type: Array, default: [] },
  appdist: [{ type: mongoose.Schema.Types.ObjectId, ref: 'Productcate'}],
  appdistobj: [{ type: mongoose.Schema.Types.ObjectId, ref: 'Productcate'}],

  is_deleted: { type: Number, default: 0 },
  is_active:  { type: Number, default: 1 },
  created_at: { type: Number, default: 0 },
  updated_at: { type: Number, default: 0 },  
}, {collection: 'advertisement'})

schema.pre('save', function(next) {  
  this.created_at = Math.floor(Date.now() / 1000)
  this.updated_at = Math.floor(Date.now() / 1000)
  next()
});

let Model = mongoose.model('Advertisement', schema)

module.exports = Model