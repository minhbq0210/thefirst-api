let mongoose = require('mongoose')
let Schema = mongoose.Schema
let Productcate = require(global.baseapp + 'models/productcateModel.js')
// create a schema
let schema = new Schema({  
  name: { type: String, default: '' },
  nameEng: { type: String, default: '' },
  slug: { type: String, default: '' },
  seotitle: { type: String, default: '' },
  seometa: { type: String, default: '' },
  desc: { type: String, default: '' },
  tags: { type: Array, default: [] },
  content: { type: String, default: ''},
  price: { type: Number, default: 0 },
  price_discount: { type: Number, default: 0 },
  price_old: { type: Number, default: 0 },
  stock: { type: Number, default: 100 },
  grams: { type: Number, default: 0 },
  sku: { type: String, default: '' },
  barcode: { type: String, default: '' },
  code: { type: String, default: '' },
  requires_shipping: { type: Number, default: 0 },
  inventory_policy: { type: Number, default: 0 },
  options: { type: Array, default: [] },
  attributes: { type: Array, default: [] },
  vendor: {type: String, default: ''},

  app: { type: mongoose.Schema.Types.ObjectId, ref: 'Productcate'},
  appobj: { type: mongoose.Schema.Types.ObjectId, ref: 'Productcate'},
  appdist: [{ type: mongoose.Schema.Types.ObjectId, ref: 'Productcate'}],
  appdistobj: [{ type: mongoose.Schema.Types.ObjectId, ref: 'Productcate'}],
  brandlist: [{ type: mongoose.Schema.Types.ObjectId, ref: 'Productcate'}],

  img_landscape: { type: String, default: '' },
  img_portrait: { type: String, default: '' },
  img_detail: { type: String, default: '' },
  image_list: { type: String, default: '' },

  ratings: {type: Array, default: []},
  ratingsSumary: {type: Number, default: 0},

  is_deleted: { type: Number, default: 0 },
  is_active:  { type: Number, default: 1 },
  is_hot:     { type: Number, default: 0 },
  created_at: { type: Number, default: 0 },
  updated_at: { type: Number, default: 0 },  
}, {collection: 'product'})


schema.pre('save', function(next) {  
  this.created_at = Math.floor(Date.now() / 1000)
  this.updated_at = Math.floor(Date.now() / 1000)
  next()
});

schema.post('findOneAndUpdate', function(doc) {
  console.log('%s has been findOneAndUpdate from the db', doc._id);
});

let Product = mongoose.model('Product', schema)
module.exports = Product