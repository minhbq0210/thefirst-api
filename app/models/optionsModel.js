let mongoose = require('mongoose')
let Schema = mongoose.Schema

// create a schema
let schema = new Schema({  
  name:  { type: String, default: '' },
  type:  { type: String, default: '' },
  key:   { type: Number, default: 0 },
  value: { type: Array, default: []},
  is_deleted: { type: Number, default: 0 },
  is_active:  { type: Number, default: 1 },
  created_at: { type: Number, default: 0 },
  updated_at: { type: Number, default: 0 } 

}, {collection: 'option'})

schema.pre('save', function(next) {  
  this.created_at = Math.floor(Date.now() / 1000)
  this.updated_at = Math.floor(Date.now() / 1000)
  next()
});


let Model = mongoose.model('Option', schema)
module.exports = Model