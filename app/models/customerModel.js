let mongoose = require('mongoose')
let Schema = mongoose.Schema
// create a schema
let schema = new Schema({  
  name: { type: String, default: '' },
  phone: { type: String, default: '' },

  address: { type: Array, default: [] },
  price: { type: Number, default: 0 },
  ids_order: { type: Array, default: [] },
  
  fee_ship: { type: Number, default: 0 },
  price_discount: { type: Number, default: 0 },
  totalpay: { type: Number, default: 0 },
  discount_bill: { type: Number, default: 0 },
  
  email: { type: String, default: ''}, 
  detail: { type: String, default: '' },
  desc: { type: String, default: '' },
  img_landscape: { type: String, default: '' },

  is_member: { type: Boolean, default: false },
  
  is_deleted: { type: Number, default: 0 },
  is_active:  { type: Number, default: 1 },
  created_at: { type: Number, default: 0 },
  updated_at: { type: Number, default: 0 },  
}, {collection: 'customer'})


schema.pre('save', function(next) {  
  this.created_at = Math.floor(Date.now() / 1000)
  this.updated_at = Math.floor(Date.now() / 1000)
  next()
});

let Customer = mongoose.model('Customer', schema)
module.exports = Customer