let mongoose = require('mongoose')
let Schema = mongoose.Schema

// type: {
//   0: cate & brand
//   1: collection
// 2: advertisement
// }
// child: {
//   0: collection cate
//   1: collection
// }
// is_brand: {
//   0: cate
//   1: brand
//   2: collect & adv
// }

// create a schema
let schema = new Schema({  
  id  : { type: Number, default: 0 },
  name: { type: String, default: '', required: true }, //, unique: true
  nameEng: { type: String, default: ''},
  slug: { type: String, default: ''},
  desc: { type: String, default: '' },
  tags: { type: Array, default: [] },
  parent_id: { type: String, default: '' },
  trace: { type: String, default: ''},
  img_landscape: { type: String, default: '' },
  priority: { type: Number, default: 0 },
  icons:      { type: String, default: ''},  
  is_deleted: { type: Number, default: 0 },
  is_active:  { type: Number, default: 1 },
  created_at: { type: Number, default: 0 },
  updated_at: { type: Number, default: 0 }, 
  is_brand: { type: Number, default: 0 }, 
  type: {type: Number, default: 0},
  child: {type: Number, default: 0},
  childArr: {type: Array, default: []},
  appdist: [{ type: mongoose.Schema.Types.ObjectId, ref: 'Productcate'}],
}, {collection: 'productcate'})

let Model = mongoose.model('Productcate', schema)

schema.pre('save', function(next) {
  let self = this  
  self.created_at = Math.floor(Date.now() / 1000)
  self.updated_at = Math.floor(Date.now() / 1000)
  next()
})

module.exports = Model