// Variables & Nodejs libs
let sTime      = 0
let http       = require('http')
let path       = require('path')
let url        = require('url')
let formidable = require('formidable')
let fs         = require('fs') 
let mongoose = require('mongoose')

// Path
global.basedir = __dirname
global.baseapp = global.basedir + '/app/'
global.baseup  = global.basedir + '/upload/'
global.baseup_img = global.baseup + 'image/'
global.basemail = global.basedir + '/emails/'
global.basemail_css = global.basemail + 'css/'

// Rx libs
global.pri     = require(global.baseapp + 'include/rxPrivate.js')
global.rxu     = require(global.baseapp + 'include/rxUtil.js')
global.rxController = require(global.baseapp + 'include/rxController.js')

let UserModel = require(global.baseapp + 'models/userModel.js')
let RoleModel = require(global.baseapp + 'models/roleModel.js')
let PermissionModel  = require(global.baseapp + 'models/permissionModel.js')

process.on('uncaughtException', function (err) {
  if ('Error: Early exit' !== err.toString()) { console.log('Caught exception: ' + err + ' line: '+ err.stack.split("\n")[4]) }
})

server = http.createServer(function (req, res) {  

  let isStatic = serverStaticFiles(req, res)
  if (!isStatic) {    
    resEnrich(req, res, function(data) {
      res.data = data
      res.setHeader('Content-Type', 'application/json')
      res.setHeader('Access-Control-Allow-Origin', '*')
      res.setHeader('Access-Control-Allow-Headers', 'Origin, X-Requested-With, Content-Type, Accept')
      let whiteList = []
      let routes    = []

      // If not in whitelist   
      if (typeof(whiteList[0]) !== 'undefined' && whiteList.indexOf(res.data.controller) == -1) {
        res.data.response({msg: 'Not support'})
      }

      // Repair controller to run 
      controllerFile = global.baseapp + 'controllers/' + res.data.controller + 'Controller.js'

      // Try handle request 
      try {
        let actionName = (req.method == 'GET')? res.data.action : req.method + res.data.action
        console.log(req.method + ' : ' + req.url)
        if (req.method == 'OPTIONS') {          
          res.data.response({success: 1, msg: 'Allow'})
        } else {
          let controllerObj = new(require(controllerFile))(res)
          checkpermission(res, function(checkpermssion) {
            if (checkpermssion == true) {       
              controllerObj[actionName]()
            } else {
              res.data.response({success: -3, msg: 'You not have permission'})
            }
          })
        }         
      } catch (ex) {
        res.data.response({msg: 'Not support'})
        throw ex        
      }
    })    
  }  
}).listen(pri.server.port)
server.timeout = pri.server.timeout

let resEnrich = function(req, res, callback) {
  let data = {}
  data.req = req
  data.url = url
  data.startTime  = rxu.now()  

  // Request related
  data.hostname   = req.headers.host
  data.pathinfo   = url.parse(req.url, true)
  data.pathname   = data.pathinfo.pathname
  data.allpath    = data.pathname.split('/')
  data.method     = req.method  
  data.controller = (typeof(data.allpath[1]) !== 'undefined') ? data.allpath[1] : 'index'
  data.action     = (typeof(data.allpath[2]) !== 'undefined') ? data.allpath[2] : 'index'   
  data.response   = function(options) {
    options  = options || {}
    
    let tempData = {}    
    let exit     = options.exit || true

    tempData.success = options.success || 0
    tempData.msg     = options.msg     || 'Request success!'    
    tempData.data    = options.data    || {}
    tempData.cpu     = (rxu.now() - data.startTime).toString().substr(0, 8) + 'ms';
    let returnData   = JSON.stringify(tempData)    
        
    if (exit || true) {      
      res.writeHead(200)
      res.end(returnData)
      res.destroy()
    } else {
      res.writeHead(200)
      res.end(returnData)
    }
  }

  // Request params 
  let form = new formidable.IncomingForm()
  form.encoding = 'utf-8'

  if (data.method === 'GET') {
    data.params = data.pathinfo.query
    callback(data)
  } else {
    form.uploadDir = global.baseup + 'temp/'
    form.parse(req, function(err, fields, files) {      
      data.params = fields
      data.files  = files
      data.form   = form      
      callback(data)
    })
  }

  // Return 
  return data
}

let serverStaticFiles = function(request, response) {
  response.setHeader("Cache-Control", "public, max-age=2592000")
  response.setHeader("Expires", new Date(Date.now() + 2592000000).toUTCString())

  let filePath = '.' + request.url
  if (filePath == './') filePath = './index.html'

  let isStatic = (filePath.indexOf('upload/image') !== -1) ? true : false
  let extname = path.extname(filePath)
  let contentType = 'text/html'
  switch (extname) {
      case '.txt':
        isStatic = true
        contentType = 'text/html'
        break;
      case '.html': 
        isStatic = true
        contentType = 'text/html'
        break;
      case '.ico':
        isStatic = true
        contentType = 'image/x-icon'
        break
      case '.js':
        isStatic = true
        contentType = 'text/javascript'
        break
      case '.css':
        isStatic = true
        contentType = 'text/css'
        break
      case '.json':
        isStatic = true
        contentType = 'application/json'
        break
      case '.png':
        isStatic = true
        contentType = 'image/png'
        break
      case '.jpg':
        isStatic = true
        contentType = 'image/jpg'
        break
      case '.wav':
        isStatic = true
        contentType = 'audio/wav'
        break
      case '.pdf':
        isStatic = true
        contentType = 'application/pdf'
        break
      case '.docx':
        isStatic = true
        contentType = 'application/vnd.openxmlformats-officedocument.wordprocessingml.document'
        break
      case '.xls':
        isStatic = true
        contentType = 'application/vnd.ms-excel'
        break
  }
  
  if (isStatic) {    
    fs.readFile(filePath, function(error, content) {      
      if (error) {
        if(error.code == 'ENOENT'){
          fs.readFile('./404.html', function(error, content) {
            response.writeHead(200, { 'Content-Type': contentType })
            response.end(content, 'utf-8')
          });
        }
        else {
          response.writeHead(500)
          response.end('Sorry, check with the site admin for error: '+error.code+' ..\n')
          response.end()
        }
      }
      else {          
        response.writeHead(200, { 'Content-Type': contentType })
        response.end(content, 'utf-8')
      }
    })

    return true;
  }  

  return false;
}

let checkpermission = async function (res, callback) {

  let arrPassController = ['site', 'authorize']
  let arrresult = []
  let controller = res.data.controller
  let action = res.data.action
  let method = res.data.method
  let strcheck = controller +'|' + method + action
  let checkper = true
  // console.log(res.data.params['authorization'], 'lll')
  // console.log(strcheck, '-----------')
  if (res.data.params['authorization'] && arrPassController.indexOf(controller) == -1) {
    let [err, newUser] =  await rxu.to(UserModel.findOne({authorization: res.data.params['authorization'], admin: true}, {}))
    let roleid = (newUser) ? newUser.roleid : null
    if (roleid) {
      // If is Admin
      let [errrole, dbarrrole] = await rxu.to(RoleModel.findOne({'_id':mongoose.Types.ObjectId(roleid), 'is_deleted': 0}))
      if (dbarrrole && dbarrrole.permission) {
        let arrper = dbarrrole.permission.split(',')
        if (arrper && arrper.length > 0) {
          let [errper, dbarrper] = await rxu.to(PermissionModel.find({'id': {'$in': arrper} }))
          let arrrole = dbarrper.map((item) => item.controller +'|'+ item.action)
          arrresult = Array.from(new Set(arrrole))
        }
      }
      if (arrresult.indexOf(strcheck) === -1) {
        checkper = false
      } 
    } 
  }

  // console.log(checkper, 'check')
  callback(checkper)
}